package ninja.slipping.pekko.persistence.hbase.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import lombok.var;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.coprocessor.ObserverContext;
import org.apache.hadoop.hbase.coprocessor.RegionCoprocessorEnvironment;
import org.apache.hadoop.hbase.coprocessor.RegionObserver;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.wal.WALEdit;

import java.io.IOException;
import java.util.List;

public class JournalIndexer implements RegionObserver {
    private static final String TABLE_NAME = "journal__pids";
    private static final String TAGS_TABLE_NAME = "journal__tags";
    private static final byte[] PID_FAMILY = "p".getBytes();
    private static final byte[] TAGS_FAMILY = "t".getBytes();

    private Connection connection = null;
    private Table pidTable = null;
    private Table tagsTable = null;

    @Override
    public void preOpen(ObserverContext<RegionCoprocessorEnvironment> c) throws IOException {
        connection = c.getEnvironment().getConnection();
        pidTable = connection.getTable(TableName.valueOf(TABLE_NAME));
        tagsTable = connection.getTable(TableName.valueOf(TAGS_TABLE_NAME));
        RegionObserver.super.preOpen(c);
    }

    @Override
    public void prePut(ObserverContext<RegionCoprocessorEnvironment> c, Put put, WALEdit edit) throws IOException {
        val cs = put.cellScanner();
        var initial = false;

        while (cs.advance()) {
            val cell = cs.current();
            val seqNr = Bytes.toLong(cell.getQualifierArray(), cell.getQualifierOffset(), cell.getQualifierLength());
            if (seqNr == 1) {
                initial = true;
                break;
            }
        }
    }

    @Override
    public void postClose(ObserverContext<RegionCoprocessorEnvironment> c, boolean abortRequested) {
        RegionObserver.super.postClose(c, abortRequested);
    }
}
