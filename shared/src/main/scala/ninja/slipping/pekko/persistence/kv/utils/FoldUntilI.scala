package ninja.slipping.pekko.persistence.kv.utils

import scala.annotation.tailrec
import scala.util.{ Failure, Success, Try }

sealed trait FoldUntilI[A] {
  def acc: A
}

object FoldUntilI {
  final case class Break[A](acc: A)    extends FoldUntilI[A]
  final case class Continue[A](acc: A) extends FoldUntilI[A]

  class FoldUntilOps[E](val iterableOnce: IterableOnce[E]) extends AnyVal {

    @inline
    final def foldUntil[A](init: A)(pf: PartialFunction[(A, E), FoldUntilI[A]]): A = {
      val it     = iterableOnce.iterator
      val lifted = pf.lift

      @tailrec
      def internalFold(curr: A): A =
        it.nextOption().flatMap(e => lifted((curr, e))) match {
          case Some(Continue(acc)) => internalFold(acc)
          case Some(Break(acc))    => acc
          case None                => curr
        }

      internalFold(init)
    }

    @inline
    def tryFoldUntil[A](init: A)(pf: PartialFunction[(A, E), Try[A]]): Try[A] = {
      val newPf: PartialFunction[(Try[A], E), FoldUntilI[Try[A]]] = {
        case (triedA, e) =>
          pf((triedA.get, e)) match {
            case f: Failure[A] => Break(f)
            case s: Success[A] => Continue(s)
          }
      }
      foldUntil[Try[A]](Success(init))(newPf)
    }
  }
}
