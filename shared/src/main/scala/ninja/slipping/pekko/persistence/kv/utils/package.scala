package ninja.slipping.pekko.persistence.kv

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

package object utils {

  @inline
  private def foldUntilIt[A, E](acc: A)(pf: PartialFunction[(A, E), Option[A]])(it: IterableOnce[E]): A = {
    val unlifted = pf.lift.andThen(_.flatten)
    val iter = it.iterator
    @tailrec
    def internalFolder(acc: A): A = {
      iter.nextOption().flatMap(n => unlifted((acc, n))) match {
        case Some(nextAcc) => internalFolder(nextAcc)
        case None => acc
      }
    }
    internalFolder(acc)
  }

  implicit class FoldUntil[E](val iterable: IterableOnce[E]) extends AnyVal {

    def foldUntil[A](acc: A)(pf: PartialFunction[(A, E), Option[A]]): A =
      foldUntilIt(acc)(pf)(iterable)

    // Maybe use cats
    def sequence[A](implicit ev: E =:= Try[A]): Try[Seq[A]] =
      new FoldUntil[Try[A]](iterable.map(ev)).tryFoldUntil(List.empty[A]) {
        case (acc, Success(element)) => Success(element :: acc)
        case (_, Failure(ex)) => Failure(ex)
      }.map(_.reverse)

    @inline
    def tryFoldUntil[A](init: => A)(f: (A, E) => Try[A]): Try[A] = {
      val it = iterable.iterator

      @tailrec
      def recAgg(current: => A): Try[A] =
        if (it.hasNext) {
          f(current, it.next()) match {
            case Failure(exception) => Failure(exception)
            case Success(value) => recAgg(value)
          }
        } else {
          Success(current)
        }

      recAgg(init)
    }
  }
}
