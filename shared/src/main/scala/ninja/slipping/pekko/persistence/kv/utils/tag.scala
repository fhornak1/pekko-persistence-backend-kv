package ninja.slipping.pekko.persistence.kv.utils


object tag {
  def apply[U]: Tagger[U] = Tagger.asInstanceOf[Tagger[U]]
  trait Tagged[U] extends Any
  type @@[+T, U] = T & Tagged[U]

  class Tagger[U] {
    def apply[T](t: T): T @@ U = t.asInstanceOf[T @@ U]
  }
  private object Tagger extends Tagger[Nothing]
}