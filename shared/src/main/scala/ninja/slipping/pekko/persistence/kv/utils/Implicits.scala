package ninja.slipping.pekko.persistence.kv.utils

object Implicits {

  implicit final class BooleanOps(val b: Boolean) extends AnyVal {
    def toOption[A](some: => A): Option[A] = ??(some)
    
    def ??[A](some: => A): Option[A] = if (b) Some(some) else None
  }

}
