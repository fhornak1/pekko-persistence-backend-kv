package ninja.slipping.pekko.presistence.kv.snapshot.dao

import org.apache.pekko.persistence.{SelectedSnapshot, SnapshotMetadata}

import scala.concurrent.Future

trait SnapshotDao {
  def saveSnapshot(metadata: SnapshotMetadata, snapshot: Any): Future[Unit]
  def latestSnapshot(persistenceId: String): Future[Option[SelectedSnapshot]]
  def snapshotForMaxSequenceNr(persistenceId: String, maxSequenceNr: Long): Future[Option[SelectedSnapshot]]
  def snapshotForMaxTimestamp(persistenceId: String, maxTimestamp: Long): Future[Option[SelectedSnapshot]]
  def snapshotForMaxSequenceNrAndMaxTimestamp(persistenceId: String, maxSequenceNr: Long, maxTimestamp: Long): Future[Option[SelectedSnapshot]]
  def delete(persistenceId: String, sequenceNr: Long): Future[Unit]
  def deleteSnapshots(persistenceId: String): Future[Unit]
  def deleteSnapshotsUpToSequenceNr(persistenceId: String, sequenceNr: Long): Future[Unit]
  def deleteSnapshotsUpToTimestamp(persistenceId: String, timestamp: Long): Future[Unit]
  def deleteSnapshotsUpToSequenceNrAndTimestamp(persistenceId: String, sequenceNr: Long, timestamp: Long): Future[Unit]
}
