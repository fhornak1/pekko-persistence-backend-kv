package ninja.slipping.pekko.presistence.kv.query.scaladsl

import ninja.slipping.pekko.presistence.kv.query.dao.KVReadJournalDao
import org.apache.pekko.NotUsed
import org.apache.pekko.persistence.query.{ EventEnvelope, Offset }
import org.apache.pekko.persistence.query.scaladsl.{
  CurrentEventsByPersistenceIdQuery,
  CurrentEventsByTagQuery,
  CurrentPersistenceIdsQuery,
  EventsByPersistenceIdQuery,
  EventsByTagQuery,
  PersistenceIdsQuery,
  ReadJournal,
}
import org.apache.pekko.stream.scaladsl.Source

class KVReadJournal
    extends ReadJournal
    with CurrentPersistenceIdsQuery
    with PersistenceIdsQuery
    with CurrentEventsByPersistenceIdQuery
    with EventsByPersistenceIdQuery
    with CurrentEventsByTagQuery
    with EventsByTagQuery {

  private val readJournalDao: KVReadJournalDao = ???

  override def currentPersistenceIds(): Source[String, NotUsed] =
    readJournalDao.currentPersistenceIds()

  override def persistenceIds(): Source[String, NotUsed] =
    readJournalDao.persistenceIds()

  override def currentEventsByPersistenceId(
    persistenceId: String,
    fromSequenceNr: Long,
    toSequenceNr: Long,
  ): Source[EventEnvelope, NotUsed] =
    readJournalDao.currentEventsByPersistenceId(persistenceId, fromSequenceNr, toSequenceNr)

  override def eventsByPersistenceId(
    persistenceId: String,
    fromSequenceNr: Long,
    toSequenceNr: Long,
  ): Source[EventEnvelope, NotUsed] =
    readJournalDao.eventsByPersistenceId(persistenceId, fromSequenceNr, toSequenceNr)

  override def currentEventsByTag(
    tag: String,
    offset: Offset,
  ): Source[EventEnvelope, NotUsed] =
    readJournalDao.currentEventsByTag(tag, offset)

  override def eventsByTag(
    tag: String,
    offset: Offset,
  ): Source[EventEnvelope, NotUsed] =
    readJournalDao.eventsByTag(tag, offset)
}
