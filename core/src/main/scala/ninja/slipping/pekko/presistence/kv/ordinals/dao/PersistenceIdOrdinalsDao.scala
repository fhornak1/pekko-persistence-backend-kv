package ninja.slipping.pekko.presistence.kv.ordinals.dao

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source

import scala.concurrent.Future

trait PersistenceIdOrdinalsDao {

  def currentPersistenceIds(): Source[String, NotUsed]

  def persistenceIds(): Source[String, NotUsed]

  def putPersistenceIds(persistenceIds: Seq[String]): Future[Long]
}
