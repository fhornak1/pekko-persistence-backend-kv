package ninja.slipping.pekko.presistence.kv.query.dao

import org.apache.pekko.persistence.query.scaladsl.{CurrentEventsByPersistenceIdQuery, CurrentEventsByTagQuery, CurrentPersistenceIdsQuery, EventsByPersistenceIdQuery, EventsByTagQuery, PersistenceIdsQuery, ReadJournal}

trait KVReadJournalDao
  extends ReadJournal
    with CurrentPersistenceIdsQuery
    with PersistenceIdsQuery
    with CurrentEventsByPersistenceIdQuery
    with EventsByPersistenceIdQuery
    with CurrentEventsByTagQuery
    with EventsByTagQuery {

}
