package ninja.slipping.pekko.presistence.kv.model

import ninja.slipping.pekko.persistence.hbase.models.model.JournalRecord

case class JournalEvent(
  persistenceId: String,
  sequenceNr: Long,
  record: JournalRecord,
  tags: Set[String] = Set.empty,
)
