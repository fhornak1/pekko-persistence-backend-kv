package ninja.slipping.pekko.presistence.kv.journal

import org.apache.pekko.Done
import org.apache.pekko.actor.{ Actor, ActorLogging, ActorRef, Stash, Status }
import org.apache.pekko.pattern.*

import scala.annotation.tailrec
import scala.concurrent.Future

object Processor {
  final case class Commit(
    tid: String,
    seqNr: Int,
  )
  final case class Rollback(
    tid: String,
    seqNr: Int,
  )
  final case class Process[R](
    tid: String,
    seqNr: Int,
    data: R,
    replyTo: ActorRef,
  )

  final case class Finalize(tid: String)

  final private case class Transaction[R](
    tid: String,
    seqNr: Int,
    data: R,
    replyTo: ActorRef,
    _status: TxnStatus = Init,
  ) {
    def isCommitted: Boolean = _status.isCommitted

    def isFinalized: Boolean = _status.isFinalized

    // TODO add IllegalStateException
    def committed: Transaction[R] = copy(_status = Committed)

    def finalized: Transaction[R] = copy(_status = Finalized)

  }

  sealed private trait TxnStatus {
    def isCommitted: Boolean = true
    def isFinalized: Boolean = false
  }
  final private case object Init extends TxnStatus {
    override def isCommitted: Boolean = false
  }
  final private case object Committed extends TxnStatus
  final private case object Finalized extends TxnStatus {
    override def isFinalized: Boolean = true
  }

  private def toTxn[R](p: Process[R]): Transaction[R] =
    Transaction(p.tid, p.seqNr, p.data, p.replyTo)

  implicit private class TxnsOps[R](val txns: List[Transaction[R]]) extends AnyVal {

    def commit(tid: String): List[Transaction[R]] =
      txns.map(txn => if (txn.tid == tid) txn.committed else txn)

    def finish(tid: String): List[Transaction[R]] =
      txns.map(txn => if (txn.tid == tid) txn.committed else txn)

    def takeCommitted: (List[Transaction[R]], List[Transaction[R]]) =
      takeUntil(txns)(_.isCommitted)

    def dropFinished: List[Transaction[R]] =
      takeUntil(txns)(_.isFinalized)._2

    def offsetInc(sizeOf: R => Int): Int =
      txns.map(txn => sizeOf(txn.data)).sum
  }

  private def takeUntil[R](lst: List[R])(p: R => Boolean): (List[R], List[R]) = {
    @tailrec
    def recTakeUntil(
      taken: List[R],
      rest: List[R],
    ): (List[R], List[R]) =
      rest match {
        case ::(head, next) if p(head) => recTakeUntil(head :: taken, next)
        case _                         => (taken.reverse, rest)
      }
    recTakeUntil(List.empty, lst)
  }

  private def remove[R](lst: List[R])(p: R => Boolean): (Option[R], List[R]) =
    (lst.find(p), lst.filterNot(p))
}

abstract class Processor[R] extends Actor with ActorLogging with Stash {
  import Processor.*
  import context.dispatcher

  private type Txns = List[Transaction[R]]
  override def receive: Receive = ???

  private def idle(
    offset: Long,
    awaitingCommit: List[Transaction[R]],
    committed: Txns,
  ): Receive = ???

  private def execProcess(
    offset: Long,
    toProcess: Txns,
    awaitingCommit: Txns,
    committed: Txns,
  ): Receive = {
    val toProc = toProcess.reverse
    process(toProc.map(_.data)).pipeTo(context.self)

    processingInProgress(offset, List.empty, toProc, awaitingCommit, committed)
  }

  private def processingInProgress(
    offset: Long,
    buffer: Txns,
    inProgress: Txns,
    awaitingCommit: Txns,
    committed: Txns,
  ): Receive = {
    case p: Process[R]        =>
      context.become(
        processingInProgress(offset, toTxn(p) :: buffer, inProgress, awaitingCommit, committed)
      )
    case Commit(tid, _)       =>
      context.become(
        processingInProgress(offset, buffer, inProgress, awaitingCommit.commit(tid), committed)
      )
    case Rollback(tid, seqNr) => // TODO
    case Finalize(tid)        =>
      context.become(
        processingInProgress(offset, buffer, inProgress, awaitingCommit, committed.finish(tid))
      )
    case Done                 =>
      inProgress.foreach { txn =>
        txn.replyTo ! Transactor.Commit(txn.tid, txn.seqNr)
      }
      val (readyToCommit, restAwaiting) = awaitingCommit.takeCommitted
      if (readyToCommit.nonEmpty) {
        context.become(
          execCommit(offset, buffer, readyToCommit, restAwaiting, committed)
        )
      }
      else if (buffer.nonEmpty) {
        context.become(
          execProcess(offset, buffer, awaitingCommit, committed)
        )
      }
      else {
        context.become(idle(offset, awaitingCommit, committed))
      }
    case Status.Failure(ex)   =>
      log.error(ex, "Failed to process transactions")
      inProgress.foreach { txn =>
        txn.replyTo ! Transactor.Abort(txn.tid, txn.seqNr, ex)
      }
      val (readyToCommit, restAwaiting) = awaitingCommit.takeCommitted
      if (readyToCommit.nonEmpty) {
        context.become(
          execCommit(offset, buffer, readyToCommit, restAwaiting, committed)
        )
      }
      else if (buffer.nonEmpty) {
        context.become(
          execProcess(offset, buffer, awaitingCommit, committed)
        )
      }
      else {
        context.become(idle(offset, awaitingCommit, committed))
      }
  }

  private def execCommit(
    offset: Long,
    buffer: Txns,
    toCommit: Txns,
    awaitingCommit: Txns,
    committed: Txns,
    seqNr: Int = 0,
  ): Receive = {
    val newOffset = offset + toCommit.offsetInc(sizeOf)
    commit(offset, newOffset, seqNr).pipeTo(context.self)

    commitingInProgress(offset, buffer, toCommit, awaitingCommit, committed, seqNr)
  }

  private def commitingInProgress(
    offset: Long,
    buffer: Txns,
    toCommit: Txns,
    awaitingCommit: Txns,
    committed: Txns,
    seqNr: Int = 0,
  ): Receive = {
    def abortAllTxns(
      tid: String,
      cause: Throwable,
    ): Unit = {
      val coBound                       = committed.size
      val toBound                       = toCommit.size + coBound
      val (writtenLeft, writtenToAbort) =
        takeUntil(committed ++ toCommit ++ awaitingCommit)(_.tid == tid)
      writtenToAbort.foreach { txn =>
        txn.replyTo ! Transactor.Abort(txn.tid, txn.seqNr, cause)
      }
      if (writtenLeft.size <= coBound) {
        val rem                    = coBound - writtenLeft.size
        val newOffset              = offset - writtenToAbort.take(rem).offsetInc(sizeOf)
        rollback(newOffset).pipeTo(context.self)
        def idleOrWriting: Receive =
          if (buffer.nonEmpty)
            execProcess(newOffset, buffer, List.empty, writtenLeft)
          else
            idle(newOffset, List.empty, writtenLeft)
        context.become {
          case Done               =>
            unstashAll()
            context.become(idleOrWriting)
          case Status.Failure(ex) =>
            log.error(
              ex,
              "Failed to rollback offset in database to: `{}`, continuing ...",
              newOffset,
            )
            unstashAll()
            context.become(idleOrWriting)
          case _                  => stash()
        }
      }
      else if (writtenLeft.size <= toBound) {
        val newToCommit = writtenLeft.drop(coBound)
        context.become(execCommit(offset, buffer, newToCommit, List.empty, committed, seqNr + 1))
      }
      else {
        val newAwaiting = writtenLeft.drop(toBound)
        context.become(commitingInProgress(offset, buffer, toCommit, newAwaiting, committed))
      }
    }

    {
      case p: Process[R]      =>
        context.become(
          commitingInProgress(
            offset,
            toTxn(p) :: buffer,
            toCommit,
            awaitingCommit,
            committed,
          )
        )
      case Commit(tid, _)     =>
        context.become(
          commitingInProgress(
            offset,
            buffer,
            toCommit,
            awaitingCommit.commit(tid),
            committed,
          )
        )
      case Rollback(tid, _)   =>
        remove(buffer)(_.tid == tid) match {
          case (Some(_), rest) =>
            context.become(commitingInProgress(offset, rest, toCommit, awaitingCommit, committed))
          case (None, _)       => abortAllTxns(tid, new Exception())
        }
      case Finalize(tid)      =>
        context.become(
          commitingInProgress(
            offset,
            buffer,
            toCommit,
            awaitingCommit,
            committed.finish(tid).dropFinished,
          )
        )
      case newOffset: Long    =>
        val (nextToCommit, restAwaiting) = awaitingCommit.takeCommitted
        val nextCommitted                = committed ++ toCommit
        if (nextToCommit.nonEmpty) {
          context.become(execCommit(newOffset, buffer, nextToCommit, restAwaiting, nextCommitted))
        }
        else if (buffer.nonEmpty) {
          context.become(execProcess(newOffset, buffer, awaitingCommit, nextCommitted))
        }
        else {
          context.become(idle(newOffset, awaitingCommit, nextCommitted))
        }
        toCommit.foreach { txn =>
          txn.replyTo ! Transactor.Commit(txn.tid, txn.seqNr)
        }
      case Status.Failure(ex) => abortAllTxns(toCommit.last.tid, ex)
    }
  }

  protected def process(messages: Seq[R]): Future[Done]

  protected def commit(
    offset: Long,
    nextOffset: Long,
    seqNr: Int,
  ): Future[(Long, Int)]

  protected def rollback(offset: Long): Future[Done]

  protected def sizeOf(element: R): Int
}
