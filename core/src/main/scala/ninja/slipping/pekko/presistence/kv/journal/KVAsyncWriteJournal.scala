package ninja.slipping.pekko.presistence.kv.journal

import com.typesafe.config.Config
import ninja.slipping.pekko.presistence.kv.PekkoSerialization
import ninja.slipping.pekko.presistence.kv.journal.dao.JournalDao
import org.apache.pekko.actor.{ ActorLogging, ActorSystem }
import org.apache.pekko.persistence.{ AtomicWrite, PersistentRepr }
import org.apache.pekko.persistence.journal.AsyncWriteJournal
import org.apache.pekko.serialization.SerializationExtension

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Success, Try }

object KVAsyncWriteJournal {
  private case class WriteFinished(
    persistenceIds: Seq[String],
    f: Future[_],
  )
}

class KVAsyncWriteJournal(config: Config) extends AsyncWriteJournal with ActorLogging {
  import KVAsyncWriteJournal.*

  private val system: ActorSystem                         = context.system
  implicit private val executionContext: ExecutionContext = context.dispatcher
  private val serialization                               = SerializationExtension(system)
  private val journalDao: JournalDao                      = ???

  private var writeInProgress: Map[String, Future[?]] = Map.empty

  override def asyncWriteMessages(messages: Seq[AtomicWrite]): Future[Seq[Try[Unit]]] = {
    val now           = System.currentTimeMillis()
    val timedMessages = messages.map(at => at.copy(payload = at.payload.map(_.withTimestamp(now))))
    val evts          = PekkoSerialization.serializeAtomicWrites(serialization)(timedMessages)

    val newPids = evts.flatMap {
      case Success(value) =>
        value.flatMap { evt =>
          if (evt.sequenceNr <= 0) Some(evt.persistenceId) else None
        }
      case _              => List.empty
    }
    val f                 = journalDao.saveMessages(timedMessages)
    val pids: Seq[String] = messages.map(_.persistenceId)
    writeInProgress = pids.map(_ -> f).toMap
    f.onComplete(_ => self ! WriteFinished(pids, f))
    f
  }

  override def asyncDeleteMessagesTo(
    persistenceId: String,
    toSequenceNr: Long,
  ): Future[Unit] =
    journalDao.deleteRecords(persistenceId, toSequenceNr)

  override def asyncReplayMessages(
    persistenceId: String,
    fromSequenceNr: Long,
    toSequenceNr: Long,
    max: Long,
  )(
    recoveryCallback: PersistentRepr => Unit
  ): Future[Unit] =
    journalDao
      .getPersistentReprs(persistenceId, fromSequenceNr, toSequenceNr, max)
      .map(_.foreach(recoveryCallback))

  override def asyncReadHighestSequenceNr(
    persistenceId: String,
    fromSequenceNr: Long,
  ): Future[Long] =
    writeInProgress
      .get(persistenceId)
      .map(_.recover { case _ => () })
      .getOrElse(Future.successful(()))
      .flatMap(_ => journalDao.findHighestSequenceNr(persistenceId, fromSequenceNr))

  override def receivePluginInternal: Receive = {
    case WriteFinished(persistenceIds, _) =>
      writeInProgress = writeInProgress -- persistenceIds
  }
}
