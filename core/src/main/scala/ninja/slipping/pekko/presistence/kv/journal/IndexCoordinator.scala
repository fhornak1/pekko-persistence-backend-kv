package ninja.slipping.pekko.presistence.kv.journal

import ninja.slipping.pekko.presistence.kv.journal.IndexCoordinator.Req
import ninja.slipping.pekko.presistence.kv.model.JournalEvent
import org.apache.pekko.actor.{ Actor, ActorRef }

object IndexCoordinator {
  final case class Req(
    tid: String,
    newPids: Seq[String],
    tags: Map[String, Seq[JournalEvent]],
    seqNr: Int,
  )

  sealed trait Vote {
    def tid: String
    def name: String
    def canCommit: Boolean
  }

  final case class Commit(
    tid: String,
    name: String
  ) extends Vote {
    override def canCommit: Boolean = true
  }

  final case class Rollback(
    tid: String,
    name: String,
  ) extends Vote {
    override def canCommit: Boolean = false
  }
}

class IndexCoordinator extends Actor {

  private var pidRef: ActorRef = ???
  private var tagRef: ActorRef = ???

  override def receive: Receive = {
    case Req(tid, newPids, tags, seqNr) => ???
  }
}
