package ninja.slipping.pekko.presistence.kv.journal.dao

import ninja.slipping.pekko.presistence.kv.model.JournalEvent
import org.apache.pekko.persistence.{AtomicWrite, PersistentRepr}

import scala.concurrent.Future
import scala.util.Try

trait JournalDao {

  def saveMessages(messages: Seq[AtomicWrite]): Future[Seq[Try[Unit]]]

  def findHighestSequenceNr(persistenceId: String, fromSeqNr: Long): Future[Long]

  def getPersistentReprs(
      persistenceId: String,
      fromSequenceNr: Long,
      toSequenceNr: Long,
      max: Long,
    ): Future[Seq[PersistentRepr]]

  def deleteRecords(persistenceId: String, toSequenceNr: Long): Future[Unit]
}
