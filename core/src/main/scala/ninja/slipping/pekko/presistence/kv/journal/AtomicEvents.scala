package ninja.slipping.pekko.presistence.kv.journal

import ninja.slipping.pekko.presistence.kv.model.JournalEvent

case class AtomicEvents(events: Seq[JournalEvent]) {
  require(events.nonEmpty, "Non empty events can not be written")
  
  def persistenceId: String = events.head.persistenceId
  
  def lowestSeqNr: Long = events.head.sequenceNr
  
  def isInitial: Boolean = lowestSeqNr <= 1
}
