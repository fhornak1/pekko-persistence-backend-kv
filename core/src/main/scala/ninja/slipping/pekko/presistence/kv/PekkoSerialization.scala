package ninja.slipping.pekko.presistence.kv

import com.google.protobuf.ByteString
import ninja.slipping.pekko.persistence.hbase.models.model.{JournalRecord, Meta}
import ninja.slipping.pekko.presistence.kv.model.{JournalEvent, StateSnapshot}
import ninja.slipping.pekko.persistence.kv.utils.*
import org.apache.pekko.persistence.journal.Tagged
import org.apache.pekko.persistence.{AtomicWrite, PersistentRepr, SelectedSnapshot, SnapshotMetadata}
import org.apache.pekko.serialization.{Serialization, Serializers}

import scala.util.{Success, Try}

object PekkoSerialization {
  case class PekkoSerialized(
    payload: ByteString,
    serId: Int,
    manifest: String,
  )


  def serializeAtomicWrites(serialization: Serialization)(atomicWrites: Seq[AtomicWrite]): Seq[Try[Seq[JournalEvent]]] =
    atomicWrites.map(_.payload.map(serializePersistentRepr(serialization)).sequence)


  private def serializePersistentRepr(serialization: Serialization)(repr: PersistentRepr): Try[JournalEvent] = {
    val (updatedPr, tags) = repr.payload match {
      case Tagged(payload, tags) =>
        (repr.withPayload(payload), tags)
      case _                     =>
        (repr, Set.empty[String])
    }

    val serializedMetadata =
      updatedPr.metadata.flatMap(meta => serialize(serialization, meta).toOption)
      serialize(serialization, repr.payload)
      .map { serializedPayload =>
        JournalRecord(
          updatedPr.writerUuid,
          updatedPr.timestamp,
          updatedPr.manifest,
          serializedPayload.payload,
          serializedPayload.serId,
          serializedPayload.manifest,
          serializedMetadata.map(meta => Meta(meta.payload, meta.serId, meta.manifest)),
        )
      }
      .map(JournalEvent(updatedPr.persistenceId, updatedPr.sequenceNr, _, tags))
  }

  final def serialize(
    serialization: Serialization,
    payload: Any): Try[PekkoSerialized] = {
    val pRef        = payload.asInstanceOf[AnyRef]
    val serializer  = serialization.findSerializerFor(pRef)
    val serManifest = Serializers.manifestFor(serializer, pRef)
    val serialized  = serialization.serialize(pRef)

    serialized.map { payload =>
      PekkoSerialized(ByteString.copyFrom(payload), serializer.identifier, serManifest)
    }
  }

  final def fromJournalEvent(
    serialization: Serialization,
    model: JournalEvent): Try[PersistentRepr] =
    for {
      payload <- serialization.deserialize(
        model.record.eventPayload.toByteArray,
        model.record.eventSerId,
        model.record.eventManifest,
      )
      meta    <- deserializeMeta(serialization, model.record.meta)
      repr = mkRepr(model.persistenceId, model.sequenceNr, model.record, payload)
    } yield meta.map(repr.withMetadata).getOrElse(repr)

  private def mkRepr(
    persistenceId: String,
    sequenceNr: Long,
    record: JournalRecord,
    payload: AnyRef): PersistentRepr =
    PersistentRepr(
      payload,
      sequenceNr,
      persistenceId,
      record.adapterManifest,
      false,
      sender = null,
      record.writer,
    ).withTimestamp(record.writerTimestamp)

  final private[kv] def fromSnapshotRecord(
    serialization: Serialization,
    snapshot: StateSnapshot): Try[SelectedSnapshot] =
    for {
      payload <- serialization.deserialize(
        snapshot.record.snapshotPayload.toByteArray,
        snapshot.record.snapshotSerId,
        snapshot.record.snapshotManifest,
      )
      meta    <- deserializeMeta(serialization, snapshot.record.meta)
    } yield SelectedSnapshot(
      SnapshotMetadata(snapshot.persistenceId, snapshot.sequenceNr, snapshot.record.created, meta),
      payload,
    )

  private def deserializeMeta(
    serialization: Serialization,
    meta: Option[Meta]): Try[Option[AnyRef]] =
    meta match {
      case Some(m) =>
        serialization.deserialize(m.payload.toByteArray, m.serId, m.manifest).map(Some(_))
      case None    => Success(None)
    }
}
