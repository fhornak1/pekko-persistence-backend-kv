package ninja.slipping.pekko.presistence.kv.journal

import org.apache.pekko.actor.{ Actor, ActorLogging, ActorRef }

import scala.collection.immutable.SortedSet

object Transactor {

  sealed trait Vote {
    def tid: String
    def seqNr: Int
  }
  final case class Commit(
    tid: String,
    seqNr: Int,
  ) extends Vote
  final case class Rollback(
    tid: String,
    seqNr: Int,
    reason: Throwable,
  ) extends Vote

  final case class Timeout(tid: String)
  final case class Complete(
    tid: String,
    seqNr: Int,
  )
  final case class Abort(
    tid: String,
    seqNr: Int,
    cause: Throwable,
  )

  final case class Process(reqs: Seq[(ActorRef, Any)])
  private val _VoteOrdering: Ordering[Vote] = Ordering.by(_.seqNr)
}

class Transactor(
  tid: String,
  coordinatorRef: ActorRef,
) extends Actor
    with ActorLogging {
  import Transactor.*
  override def receive: Receive = {
    case Process(reqs) =>
      val refs = reqs.map(_._1)
      reqs.zipWithIndex.foreach {
        case ((ref, msg), idx) =>
          ref ! Processor.Process(tid, idx, msg, context.self)
      }
      // TODO add timeout
      context.become(preparing(SortedSet.empty[Vote](_VoteOrdering), refs))
  }

  private def preparing(
    votes: SortedSet[Vote],
    refs: Seq[ActorRef],
  ): Receive = {
    case v @ Commit(`tid`, _)             =>
      val newVotes = votes + v
      if (isComplete(newVotes)) {
        context.become(prepared(refs))
      }
      else {
        context.become(preparing(newVotes, refs))
      }
    case v @ Rollback(tid, seqNr, reason) =>
      refs.zipWithIndex.foreach {
        case (ref, seqNr) =>
          ref ! Processor.Rollback(tid, seqNr)
      }
      context.become(aborting(refs))
  }

  private def isComplete(votes: SortedSet[Vote]): Boolean =
    votes.zipWithIndex.forall { case (v, idx) => v.seqNr == idx }

  private def prepared(refs: Seq[ActorRef]): Receive = {
    refs.zipWithIndex.foreach {
      case (ref, seqNr) =>
        ref ! Processor.Commit(tid, seqNr)
    }
    commiting(SortedSet.empty(_VoteOrdering), refs)
  }

  private def commiting(
    votes: SortedSet[Vote],
    refs: Seq[ActorRef],
  ): Receive = {
    case v @ Commit(`tid`, _)               =>
      val newVotes = votes + v
      if (isComplete(newVotes)) {
        coordinatorRef ! Coordinator.Complete(tid)
        refs.foreach { proc =>
          proc ! Processor.Finalize(tid)
        }
        context.stop(self)
      }
      else {
        context.become(commiting(newVotes, refs))
      }
    case v @ Rollback(`tid`, seqNr, reason) =>
  }

  private def aborting(refs: Seq[ActorRef]): Receive = ???

  private def comited(): Receive = ???

  private def aborted(): Receive = ???

}
