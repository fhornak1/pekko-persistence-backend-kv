package ninja.slipping.pekko.presistence.kv.state.scaladsl

import org.apache.pekko.{Done, NotUsed}
import org.apache.pekko.persistence.query.{DurableStateChange, Offset}
import org.apache.pekko.persistence.query.scaladsl.DurableStateStoreQuery
import org.apache.pekko.persistence.state.scaladsl.{DurableStateUpdateStore, GetObjectResult}
import org.apache.pekko.stream.scaladsl.Source

import scala.concurrent.Future

class DurableStateStore[A] extends DurableStateUpdateStore[A] with DurableStateStoreQuery[A] {
  override def upsertObject(persistenceId: String, revision: Long, value: A, tag: String): Future[Done] = ???

  override def deleteObject(persistenceId: String): Future[Done] = ???

  override def deleteObject(persistenceId: String, revision: Long): Future[Done] = ???

  override def currentChanges(tag: String, offset: Offset): Source[DurableStateChange[A], NotUsed] = ???

  override def changes(tag: String, offset: Offset): Source[DurableStateChange[A], NotUsed] = ???

  override def getObject(persistenceId: String): Future[GetObjectResult[A]] = ???
}
