package ninja.slipping.pekko.presistence.kv.tags

import ninja.slipping.pekko.presistence.kv.journal.Processor
import ninja.slipping.pekko.presistence.kv.model.JournalEvent
import org.apache.pekko.Done
import org.apache.pekko.actor.{ActorRef, Props}

import scala.concurrent.Future

object TagWriter {
  def props(tagName: String): Props = Props(classOf[TagWriter], tagName)
}

class TagWriter(tag: String) extends Processor[Seq[JournalEvent]] {
  override protected def process(messages: Seq[Seq[JournalEvent]]): Future[Done] = ???

  override protected def commit(offset: Long, nextOffset: Long, seqNr: Int): Future[(Long, Int)] = ???

  override protected def rollback(offset: Long): Future[Done] = ???

  override protected def sizeOf(element: Seq[JournalEvent]): Int =
    element.size
}
