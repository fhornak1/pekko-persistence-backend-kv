package ninja.slipping.pekko.presistence.kv.model

import ninja.slipping.pekko.persistence.hbase.models.model.SnapshotRecord

case class StateSnapshot(persistenceId: String, sequenceNr: Long, record: SnapshotRecord)
