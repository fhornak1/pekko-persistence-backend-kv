package ninja.slipping.pekko.presistence.kv.pids

import ninja.slipping.pekko.presistence.kv.journal.Processor
import org.apache.pekko.Done
import org.apache.pekko.actor.Props

import scala.concurrent.Future

object PidWriterProcessor {

  def props(): Props = Props(classOf[PidWriterProcessor])
}

class PidWriterProcessor extends Processor[Seq[String]] {
  override protected def process(messages: Seq[Seq[String]]): Future[Done] = ???

  override protected def commit(offset: Long, nextOffset: Long, seqNr: Int): Future[(Long, Int)] = ???

  override protected def rollback(offset: Long): Future[Done] = ???

  override protected def sizeOf(element: Seq[String]): Int = element.size
}
