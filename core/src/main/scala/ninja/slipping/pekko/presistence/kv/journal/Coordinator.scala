package ninja.slipping.pekko.presistence.kv.journal

import ninja.slipping.pekko.presistence.kv.tags.TagWriter
import org.apache.pekko.actor.{Actor, ActorRef}

object Coordinator {

  final case class WriteEvents(events: Seq[AtomicEvents])

  final case class Complete(tid: String)
  final case class Failed(
    tid: String,
    reason: Throwable,
  )

}

class Coordinator extends Actor {
  import Coordinator.*

  private var tagRefs: Map[String, ActorRef]      = ???
  private var evtWriter: ActorRef                 = ???
  private var pidWritter: ActorRef                = ???
  private var transactions: Map[String, ActorRef] = ???

  override def receive: Receive = {
    case WriteEvents(events) => ???
    case Complete(tid)       => ???
    case Failed(tid, ex)     => ???
  }

  private def getOrCrateTagWriter(tag: String): ActorRef =
    tagRefs.getOrElse(tag, context.actorOf(TagWriter.props(tag), s"tag-writer-$tag"))
}
