import Dependencies.*
lazy val scala212               = "2.12.18"
lazy val scala213               = "2.13.12"
lazy val scala3                 = "3.3.0"
lazy val supportedScalaVersions = Seq(scala212, scala213, scala3)

ThisBuild / scalaVersion := scala213
ThisBuild / version      := "0.1.0-SNAPSHOT"
ThisBuild / organization := "ninja.slipping.pekko"

lazy val root = (project in file("."))
  .aggregate(core, hbase)
  .settings(
    update / aggregate := false,
    publish / skip     := true,
  )

lazy val shared = Project(id = "shared", base = file("shared"))
  .settings(
    name               := "pekko-persistence-backend-kv-shared",
    crossScalaVersions := supportedScalaVersions,
    Compile / scalacOptions ++= {
      CrossVersion.partialVersion(scalaVersion.value) match {
        case Some((2, n)) if n >= 12 => scala2Options
        case Some((3, _))            => scala3Options
        case _                       => Nil
      }
    },
  )

lazy val core = Project(id = "core", base = file("core"))
  .settings(
    name                 := "pekko-persistence-backend-kv-core",
    crossScalaVersions   := supportedScalaVersions,
    libraryDependencies ++= Seq(
      Pekko.`pekko-actor`,
      Pekko.`pekko-persistence`,
      Pekko.`pekko-persistence-query`,
      Protobuf.`scalapb-runtime` % "protobuf",
      Common.`slf4j-api`.force(),
      Scala.`scala-logging`,
    ),
    libraryDependencies ++= Scala.scalaTest,
    Compile / PB.protoSources += file("core/src/main/protobuf"),
    Compile / PB.targets := Seq(
      scalapb.gen() -> (Compile / sourceManaged).value / "scalapb"
    ),
    Compile / scalacOptions ++= {
      CrossVersion.partialVersion(scalaVersion.value) match {
        case Some((2, n)) if n >= 12 => scala2Options
        case Some((3, _))            => scala3Options
        case _                       => Nil
      }
    },
  ).dependsOn(shared)

lazy val hbase = Project(id = "hbase", base = file("hbase"))
  .settings(
    name               := "pekko-persistence-backend-hbase",
    crossScalaVersions := supportedScalaVersions,
    libraryDependencies ++= Seq(
      Pekko.`pekko-actor`,
      Pekko.`pekko-persistence`,
      HBase.`hbase-shaded-client`,
      Common.`slf4j-api`.force(),
      Scala.`scala-logging`,
      Pekko.`pekko-testkit`         % Test,
      Pekko.`pekko-persistence-tck` % Test,
      Pekko.`pekko-stream-testkit`  % Test,
      HBase.`hbase-it`              % Test,
    ),
    libraryDependencies ++= Scala.scalaTest ++ Log4j.testBundle,
    Compile / scalacOptions ++= {
      CrossVersion.partialVersion(scalaVersion.value) match {
        case Some((2, n)) if n >= 12 => scala2Options
        case Some((3, _))            => scala3Options
        case _                       => Nil
      }
    },
  )
  .dependsOn(core, shared)

lazy val hbaseCoprocessorShared = (project in file("hbase-coprocessor-shared"))
  .settings(
    javacOptions ++= javaOpts,
    name                                 := "pekko-persistence-backend-hbase-coprocessor-shared",
    compileOrder                         := CompileOrder.JavaThenScala,
    crossPaths                           := false,
    autoScalaLibrary                     := false,
    Compile / unmanagedSourceDirectories := (Compile / javaSource).value :: Nil,
    Test / unmanagedSourceDirectories    := (Test / javaSource).value :: Nil,
    libraryDependencies ++= Seq(
      Common.`slf4j-api`,
      HBase.`hbase-common`,
      HBase.`hbase-client` % Provided,
      Java.lombok          % Provided,
    ),
  )

lazy val hbaseCoprocessorServer = (project in file("hbase-coprocessor-server"))
  .settings(
    javacOptions ++= javaOpts,
    name                                 := "pekko-persistence-backend-hbase-coprocessor-server",
    crossPaths                           := false,
    autoScalaLibrary                     := false,
    Compile / unmanagedSourceDirectories := (Compile / javaSource).value :: Nil,
    Test / unmanagedSourceDirectories    := (Test / javaSource).value :: Nil,
    libraryDependencies ++= Seq(
      Common.`slf4j-api`,
      HBase.`hbase-common` % Provided,
      HBase.`hbase-client` % Provided,
      HBase.`hbase-server` % Provided,
      Java.lombok          % Provided,
    ),
    libraryDependencies ++= Java.`junit-jupiter-tests`,
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.

lazy val javaOpts = Seq(
  "-source",
  "1.8",
  "-target",
  "1.8",
  "-Xlint:unchecked",
  "-Xlint:deprecation",
)

lazy val scala2Options = sharedScalacOptions ++
  Seq("-target:jvm-1.8", "-Xsource:3", "-Yscala3-implicit-resolution", "-explaintypes")

lazy val scala3Options = sharedScalacOptions ++
  Seq("-Xunchecked-java-output-version:8", "-explain")

lazy val sharedScalacOptions =
  Seq(
    "-encoding",
    "UTF-8",
    "-Wunused:imports,privates,locals",
    "-feature",
    "-unchecked",
    "-language:higherKinds",
  )
