package ninja.slipping.pekko.persistence.hbase.persistenceids.dao

import ninja.slipping.pekko.persistence.hbase.conf.HBasePersistenceIdOrdinalConf
import ninja.slipping.pekko.persistence.hbase.persistenceids.models.PersistenceIdModel
import ninja.slipping.pekko.persistence.hbase.utils.Implicits.*
import org.apache.hadoop.hbase.client.Result

import java.util.concurrent.CompletionStage
import scala.util.{Success, Try}

class PersistenceIdMapper(conf: HBasePersistenceIdOrdinalConf) {

  def parseHighestPersistenceId(result: Result): CompletionStage[Long] =
    result.parseRowOption[Long].getOrElse(Success(0L)).toCompletedStage

  def parsePersistenceId(result: Result): Try[PersistenceIdModel] =
    for {
      ordinal       <- result.parseRow[Long]
      persistenceId <- result.parseLatestCell[String](conf.family, conf.persistenceIdCQ)
    } yield PersistenceIdModel(ordinal, persistenceId)

}
