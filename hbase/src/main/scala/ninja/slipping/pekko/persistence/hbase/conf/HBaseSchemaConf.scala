package ninja.slipping.pekko.persistence.hbase.conf

case class HBaseSchemaConf(family: Array[Byte])
