package ninja.slipping.pekko.persistence.hbase.tags.models

import ninja.slipping.pekko.persistence.hbase.models.model.JournalRecord

case class TagModel(
  tag: String,
  ordinal: Long,
  persistenceId: String,
  sequenceNr: Long,
  entry: JournalRecord,
)
