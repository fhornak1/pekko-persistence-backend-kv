package ninja.slipping.pekko.persistence.hbase.journal.dao

case class HBaseSchemaConf(
  tableName: String,
  journalFamily: Array[Byte],
  metaFamily: Array[Byte],
  snapshotFamily: Array[Byte],
  tagFamily: Array[Byte],
  tagPersistenceIdCol: Array[Byte],
  tagSequenceNrCol: Array[Byte],
  tagRecordCol: Array[Byte]
)
