package ninja.slipping.pekko.persistence.hbase.stream.impl

import com.typesafe.scalalogging.Logger
import ninja.slipping.pekko.persistence.hbase.stream.errors.Errors
import org.apache.hadoop.hbase.client.{ AdvancedScanResultConsumer, Result }
import org.apache.pekko.stream.stage.AsyncCallback

import java.util.concurrent.atomic.AtomicReference
import scala.concurrent.Await
import scala.concurrent.duration.*

object HBaseAsyncScanCallback {

  final private[impl] class Controller {
    private val log = Logger[Controller]

    @volatile private var suspend = false
    @volatile private var close   = false

    private val resumerRef =
      new AtomicReference[Option[AdvancedScanResultConsumer.ScanResumer]](None)

    def signalSuspend(): Unit = {
      log.info("Setting suspend flag.")
      suspend = true
    }

    def signalClose(): Unit = {
      log.info("Setting closed flag.")
      close = true
    }

    def resume(): Unit =
      log.debug("Called resume scan")
    resumerRef.getAndUpdate(_ => None) match {
      case Some(resumer) =>
        log.info("Resuming scan.")
        resumer.resume()
      case None          =>
        log.debug("Resume called, but scan is not suspended, ScanResumer is not set!")
    }

    def isSuspended: Boolean = resumerRef.get().isDefined

    def isClosed: Boolean = close

    def setScanResumer(resumer: => AdvancedScanResultConsumer.ScanResumer): Unit =
      if (suspend && !isSuspended) {
        resumerRef.getAndUpdate(_ => None) match {
          case Some(value) => resumerRef.set(Some(value))
          case None        => resumerRef.set(Some(resumer))
        }
      }
  }
}

final private[impl] class HBaseAsyncScanCallback(
  onNext: AsyncCallback[Array[Result]],
  onFailure: AsyncCallback[Throwable],
  onComplete: AsyncCallback[Unit],
  controller: HBaseAsyncScanCallback.Controller,
  idleScanTimeout: FiniteDuration,
) extends AdvancedScanResultConsumer {

  private val log          = Logger[HBaseAsyncScanCallback]
  private var lastOnNextMs = System.currentTimeMillis()

  override def onNext(
    results: Array[Result],
    scanController: AdvancedScanResultConsumer.ScanController,
  ): Unit = {
    lastOnNextMs = System.currentTimeMillis()
    if (controller.isClosed) {
      log.info("Terminating scan and returning.")
      scanController.terminate()
    }
    else {
      log.trace("Invoking onNext callback and waiting for 1 second")
      Await.ready(onNext.invokeWithFeedback(results), 1.second)

      controller.setScanResumer(() => scanController.suspend())
    }
  }

  override def onError(throwable: Throwable): Unit = {
    controller.signalClose()
    log.warn("Scan completed with error. Invoking onFailure callback.", throwable)
    onFailure.invoke(throwable)
  }

  override def onComplete(): Unit = {
    controller.signalClose()
    log.info("Scan completed successfully. Invoking onComplete callback.")
    onComplete.invoke(())
  }

  override def onHeartbeat(scanController: AdvancedScanResultConsumer.ScanController): Unit =
    if (controller.isClosed) {
      log.info("Terminating scan.")
      scanController.terminate()
    }
    else {
      val durationSinceOnNext = System.currentTimeMillis() - lastOnNextMs
      log.debug(s"Duration since last onNext: $durationSinceOnNext [ms]")

      if (durationSinceOnNext > idleScanTimeout.toMillis) {
        log.warn(
          s"Terminating scan due to period since last Results emission (${durationSinceOnNext} [ms]) " +
            s"is greater than predefined idleScanTimeout (${idleScanTimeout})"
        )
        controller.signalClose()
        scanController.terminate()
        onFailure.invoke(Errors.waitingForOnNextTimedOut(durationSinceOnNext))
      }
    }
}
