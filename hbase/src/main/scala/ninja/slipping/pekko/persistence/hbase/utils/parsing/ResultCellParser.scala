package ninja.slipping.pekko.persistence.hbase.utils.parsing

import org.apache.hadoop.hbase.Cell

import scala.util.Try

trait ResultCellParser[R, T] extends CellParser[T] {

  def apply(cell: Cell): Try[T] =
    parseRow(cell).flatMap(parse(_, cell))

  def parseRow(cell: Cell): Try[R]

  def parse(
    row: R,
    cell: Cell,
  ): Try[T]

  def createParser(row: R): CellParser[T] =
    (cell: Cell) => parse(row, cell)
}
