package ninja.slipping.pekko.persistence.hbase.journal.dao

import ninja.slipping.pekko.persistence.hbase.Encoders
import ninja.slipping.pekko.persistence.hbase.models.model.{JournalRecord, Meta}
import ninja.slipping.pekko.persistence.hbase.utils.Implicits.*
import ninja.slipping.pekko.persistence.kv.utils.FoldUntil
import ninja.slipping.pekko.presistence.kv.PekkoSerialization
import ninja.slipping.pekko.presistence.kv.model.JournalEvent
import org.apache.hadoop.hbase.Cell
import org.apache.hadoop.hbase.client.Result
import org.apache.pekko.persistence.{AtomicWrite, PersistentRepr}
import org.apache.pekko.persistence.journal.Tagged
import org.apache.pekko.serialization.Serialization

import java.util.List as JList
import java.util.concurrent.{CompletableFuture, CompletionStage}
import scala.util.Try

class HBaseJournalMapper(serialization: Serialization) {

  def parseHighestSequenceNr(resultLst: JList[Result]): CompletionStage[Long] =
    if (resultLst.size() == 1 && resultLst.get(0).size() == 1) {
      val cells = resultLst.get(0).rawCells()
      parseQualifierSequenceNr(cells(0)).toCompletedStage
    }
    else {
      CompletableFuture.failedFuture(
        new RuntimeException(
          "Expected only one cell, please check if appropriate query was called!"
        )
      )
    }
    
  def parseSequenceNrs(result: Result): CompletionStage[Seq[Long]] =
    result.rawCells().map(parseQualifierSequenceNr).toSeq.sequence.toCompletedStage

  private def parseQualifierSequenceNr(cell: Cell): Try[Long] =
    Encoders
      .decodeSequenceNr(cell.getQualifierArray, cell.getQualifierOffset, cell.getQualifierLength)


  def serializeAtomicWrites(atomicWrites: Seq[AtomicWrite]): Seq[Try[Seq[JournalEvent]]] =
    atomicWrites.map(_.payload.map(serializePersistentRepr).sequence)


  private def serializePersistentRepr(repr: PersistentRepr): Try[JournalEvent] = {
    val (updatedPr, tags) = repr.payload match {
      case Tagged(payload, tags) =>
        (repr.withPayload(payload), tags)
      case _                     =>
        (repr, Set.empty[String])
    }

    val serializedMetadata =
      updatedPr.metadata.flatMap(meta => PekkoSerialization.serialize(serialization, meta).toOption)
    PekkoSerialization
      .serialize(serialization, repr.payload)
      .map { serializedPayload =>
        JournalRecord(
          updatedPr.writerUuid,
          updatedPr.timestamp,
          updatedPr.manifest,
          serializedPayload.payload,
          serializedPayload.serId,
          serializedPayload.manifest,
          serializedMetadata.map(meta => Meta(meta.payload, meta.serId, meta.manifest)),
        )
      }
      .map(JournalEvent(updatedPr.persistenceId, updatedPr.sequenceNr, _, tags))
  }

  def parsePersistentReprs(persistenceId: String)(result: Result): CompletionStage[Seq[PersistentRepr]] =
    result
      .rawCells()
      .toSeq
      .map { cell =>
        for {
          event          <- cell.parse[JournalEvent].map(_.copy(persistenceId = persistenceId))
          persistentRepr <- PekkoSerialization.fromJournalEvent(serialization, event)
        } yield persistentRepr
      }
      .sequence
      .toCompletedStage
}
