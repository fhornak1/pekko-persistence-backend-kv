package ninja.slipping.pekko.persistence.hbase.utils.parsing

import com.google.protobuf.CodedInputStream
import ninja.slipping.pekko.persistence.hbase.Encoders
import ninja.slipping.pekko.persistence.hbase.models.model.{JournalRecord, SnapshotRecord}
import ninja.slipping.pekko.presistence.kv.model.JournalEvent
import org.apache.hadoop.hbase.Cell
import org.apache.hadoop.hbase.util.Bytes

import scala.util.Try

trait CellParser[T] {
  def apply(cell: Cell): Try[T]
}

object CellParser {
  def apply[T](
    implicit ev: CellParser[T]
  ): CellParser[T] = ev

  implicit val longValParser: CellParser[Long] = (cell: Cell) =>
    Try(Bytes.toLong(cell.getValueArray, cell.getValueOffset, cell.getValueLength))

  implicit val strValParser: CellParser[String] = (cell: Cell) =>
    Try(Encoders.toString(cell.getValueArray, cell.getValueOffset, cell.getValueLength))

  implicit val journalRecordParser: CellParser[JournalRecord] = (cell: Cell) =>
    Try(
      JournalRecord.parseFrom(
        CodedInputStream.newInstance(cell.getValueArray, cell.getValueOffset, cell.getValueLength)
      )
    )
  
  implicit val journalEventParser: CellParser[JournalEvent] = (cell: Cell) =>
    for {
      sequenceNr <- Encoders.decodeSequenceNr(cell.getQualifierArray, cell.getQualifierOffset, cell.getQualifierLength)
      recrod <- journalRecordParser(cell)
    } yield JournalEvent("", sequenceNr, recrod, Set.empty)

  implicit val snapshotRecordParser: CellParser[SnapshotRecord] = (cell: Cell) =>
    Try(
      SnapshotRecord.parseFrom(
        CodedInputStream.newInstance(cell.getValueArray, cell.getValueOffset, cell.getValueLength)
      )
    )
}
