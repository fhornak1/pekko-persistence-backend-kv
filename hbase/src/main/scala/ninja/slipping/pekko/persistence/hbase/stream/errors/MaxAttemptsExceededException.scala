package ninja.slipping.pekko.persistence.hbase.stream.errors

class MaxAttemptsExceededException(message: String) extends RuntimeException(message)
