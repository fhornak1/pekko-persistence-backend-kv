package ninja.slipping.pekko.persistence.hbase.journal.dao

import ninja.slipping.pekko.persistence.hbase.Encoders
import ninja.slipping.pekko.presistence.kv.model.JournalEvent
import org.apache.hadoop.hbase.client.{CheckAndMutate, Delete, Get, Put, Scan}
import org.apache.hadoop.hbase.filter.*

import scala.jdk.CollectionConverters.*

import java.util.{ArrayList as JArrayList, List as JList}
import scala.util.{Success, Try}

class HBaseJournalQueries(conf: HBaseSchemaConf) {

  def checkNewPersistenceIds(
    persistenceIds: Seq[String],
  ): JList[Get] =
    persistenceIds.map { persistenceId =>
      new Get(Encoders.encodePersistenceId(persistenceId)).addFamily(conf.journalFamily).setFilter(new FirstKeyOnlyFilter)
    }.asJava

  def putJournalEvents(events: Seq[Try[Seq[JournalEvent]]]): (JList[Put], Seq[Try[Unit]]) = {
    val puts: JList[Put] = events.foldLeft(new JArrayList[Put](events.size)) {
      case (list, Success(events)) =>
        list.addLast(putJournalEvts(events))
        list
      case (list, _)               => list
    }

    (puts, events.map(_.map(_ => ())))
  }

  private def putJournalEvts(events: Seq[JournalEvent]): Put = {
    val put = new Put(Encoders.encodePersistenceId(events.head.persistenceId))

    events.foldLeft(put) {
      case (put, event) =>
        put.addColumn(
          conf.journalFamily,
          Encoders.encodeSequenceNr(event.sequenceNr),
          event.record.toByteArray,
        )
    }
  }

  def getHighestSequenceNr(persistenceId: String): Scan = {
    val startRow = Encoders.encodePersistenceId(persistenceId)
    new Scan()
      .withStartRow(startRow)
      .withStopRow(startRow, true)
      .addFamily(conf.journalFamily)
      .setReversed(true)
      .setOneRowLimit()
      .setFilter(
        new FilterList(FilterList.Operator.MUST_PASS_ALL, new FirstKeyOnlyFilter, new KeyOnlyFilter)
      )
  }

  def getJournalRecords(
    persistenceId: String,
    fromSequenceNr: Long,
    toSequenceNr: Long,
    max: Long,
  ): Get =
    new Get(Encoders.encodePersistenceId(persistenceId))
      .addFamily(conf.journalFamily)
      .setFilter(
        new FilterList(
          FilterList.Operator.MUST_PASS_ALL,
          new ColumnRangeFilter(
            Encoders.encodeSequenceNr(fromSequenceNr),
            true,
            Encoders.encodeSequenceNr(toSequenceNr),
            true,
          ),
          new ColumnCountGetFilter(if (max > Int.MaxValue) Int.MaxValue else max.toInt),
        )
      )

  def getSequenceNrsUpTo(
    persistenceId: String,
    toSequenceNr: Long,
  ): Get =
    new Get(Encoders.encodePersistenceId(persistenceId))
      .addFamily(conf.journalFamily)
      .setFilter(
        new FilterList(
          FilterList.Operator.MUST_PASS_ALL,
          new ColumnRangeFilter(
            Encoders.encodeSequenceNr(0),
            true,
            Encoders.encodeSequenceNr(toSequenceNr),
            true,
          ),
          new KeyOnlyFilter(),
        )
      )

  def deleteSequenceNrs(
    persistenceId: String,
    seqNrs: Seq[Long],
  ): Delete =
    seqNrs.foldLeft(new Delete(Encoders.encodePersistenceId(persistenceId))) {
      case (delete, seqNr) =>
        delete.addColumn(conf.journalFamily, Encoders.encodeSequenceNr(seqNr))
    }

}
