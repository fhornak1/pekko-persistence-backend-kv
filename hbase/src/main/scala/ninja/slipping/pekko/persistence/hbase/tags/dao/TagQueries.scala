package ninja.slipping.pekko.persistence.hbase.tags.dao

import ninja.slipping.pekko.persistence.hbase.Encoders
import ninja.slipping.pekko.persistence.hbase.journal.dao.HBaseSchemaConf
import ninja.slipping.pekko.persistence.hbase.tags.models.TagModel
import ninja.slipping.pekko.presistence.kv.model.JournalEvent
import org.apache.hadoop.hbase.client.{CheckAndMutate, Get, Increment, Put, Scan}
import org.apache.hadoop.hbase.filter.PageFilter

import java.util.{ArrayList as JArrayList, List as JList}
import scala.annotation.tailrec

class TagQueries(conf: HBaseSchemaConf) {
  import TagsSchema.*

  def putEvents(
    tag: String,
    events: Seq[JournalEvent],
    ordinal: Long,
  ): JList[Put] = {
    @tailrec
    def appendHeadElem(
      ordinal: Long,
      lst: List[JournalEvent],
      collector: JList[Put],
    ): JList[Put] =
      lst match {
        case Nil          => collector
        case head :: tail =>
          collector.add(
            new Put(Encoders.encodeTag(tag, ordinal))
              .addColumn(
                conf.tagFamily,
                PersistenceIdColumn,
                Encoders.strToBytes(head.persistenceId),
              )
              .addColumn(
                conf.tagFamily,
                SequenceNrColumn,
                Encoders.encodeSequenceNr(head.sequenceNr),
              )
              .addColumn(conf.tagFamily, RecordColumn, head.record.toByteArray)
          )
          appendHeadElem(ordinal + 1, tail, collector)
      }

    appendHeadElem(ordinal, events.toList, new JArrayList[Put](events.size))
  }
  
  def getLastCommittedOrdinal(tag: String): Get =
    new Get(Encoders.encodeTag(tag, 0L))
      .addColumn(conf.tagFamily, OrdinalColumn)
      
  def checkAndMutateHighestOrdinal(tag: String, previous: Long, newOrd: Long): CheckAndMutate =
    CheckAndMutate.newBuilder(Encoders.encodeTag(tag, 0L))
      .ifEquals(conf.tagFamily, OrdinalColumn, Encoders.encodeSequenceNr(previous))
      .build(
        new Put(Encoders.encodeTag(tag, 0L))
          .addColumn(conf.tagFamily, OrdinalColumn, Encoders.encodeSequenceNr(newOrd))
      )
}
