package ninja.slipping.pekko.persistence.hbase.utils.parsing

import ninja.slipping.pekko.persistence.hbase.Encoders
import org.apache.hadoop.hbase.Cell

import scala.util.Try

trait RowParser[T] {
  def apply(cell: Cell): Try[T]
}

object RowParser {
  
  def apply[T](implicit parser: RowParser[T]): RowParser[T] = parser
  
  
  implicit val longRowParser: RowParser[Long] = (cell: Cell) =>
    Encoders.decodeSequenceNr(cell.getRowArray, cell.getValueOffset, cell.getValueLength)
  
  implicit val stringRowParser: RowParser[String] = (cell: Cell) =>
    Try(Encoders.toString(cell.getRowArray, cell.getRowOffset, cell.getRowLength))
}
