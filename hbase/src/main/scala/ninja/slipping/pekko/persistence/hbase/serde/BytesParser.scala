package ninja.slipping.pekko.persistence.hbase.serde

import org.apache.hadoop.hbase.util.Bytes

import java.nio.charset.StandardCharsets
import scala.util.Try

trait BytesParser[T] {

  def tryParse(
    array: Array[Byte],
    offset: Int,
    length: Int,
  ): Try[T] = Try(parse(array, offset, length))

  def parse(
    array: Array[Byte],
    offset: Int,
    length: Int,
  ): T
}

object BytesParser {

  def apply[T](
    implicit bp: BytesParser[T]
  ): BytesParser[T] = bp

  implicit val stringParser: BytesParser[String] = (
    array: Array[Byte],
    offset: Int,
    length: Int,
  ) => new String(array, offset, length, StandardCharsets.UTF_8)

  implicit val intParser: BytesParser[Int] = (
    array: Array[Byte],
    offset: Int,
    length: Int,
  ) => Bytes.toInt(array, offset, length)

  implicit val longParser: BytesParser[Long] = (
    array: Array[Byte],
    offset: Int,
    length: Int,
  ) => Bytes.toLong(array, offset, length)
}
