package ninja.slipping.pekko.persistence.hbase.stream.scaladsl

import ninja.slipping.pekko.persistence.hbase.stream.HBaseAsyncScanProperties
import ninja.slipping.pekko.persistence.hbase.stream.impl.HBaseAsyncScanSourceStage
import org.apache.hadoop.hbase.client.{ AdvancedScanResultConsumer, AsyncTable, Result, Scan }
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source

object HBaseStage {

  /** Reads elements from HBase.
    * @param table
    *   HBase AsyncTable that will be scanned
    * @param scan
    *   request.
    * @return
    *   Source
    */
  def source(
    table: AsyncTable[? <: AdvancedScanResultConsumer],
    scan: Scan,
    properties: HBaseAsyncScanProperties = HBaseAsyncScanProperties.default(),
  ): Source[Result, NotUsed] =
    Source.fromGraph(HBaseAsyncScanSourceStage(table, scan, properties))
}
