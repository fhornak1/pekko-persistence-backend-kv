package ninja.slipping.pekko.persistence.hbase.journal.dao

import ninja.slipping.pekko.persistence.hbase.persistenceids.dao.PersistenceIdDao
import ninja.slipping.pekko.persistence.hbase.tags.dao.TagsDao
import ninja.slipping.pekko.presistence.kv.journal.dao.JournalDao
import ninja.slipping.pekko.presistence.kv.model.JournalEvent
import org.apache.hadoop.hbase.client.{ AdvancedScanResultConsumer, AsyncConnection, AsyncTable }
import org.apache.pekko.persistence.{ AtomicWrite, PersistentRepr }

import scala.concurrent.{ ExecutionContext, Future }
import scala.jdk.FutureConverters.CompletionStageOps
import scala.jdk.CollectionConverters.*
import scala.util.Try

class HBaseJournalDao(asyncConnection: AsyncConnection) extends JournalDao {

  implicit private val ec: ExecutionContext = ???

  private val journal: AsyncTable[AdvancedScanResultConsumer] = ???
  private val queries: HBaseJournalQueries                    = ???
  private val mapper: HBaseJournalMapper                      = ???
  private val pidDao: PersistenceIdDao                        = ???
  private val tagDao: TagsDao                                 = ???

  override def saveMessages(messages: Seq[AtomicWrite]): Future[Seq[Try[Unit]]] = {
    val events                      = mapper.serializeAtomicWrites(messages)
    val (puts, serializationResult) = queries.putJournalEvents(events)
    for {
      newPids <- listNewPersistenceIds(events)
      _       <- journal.putAll(puts).asScala
    } yield serializationResult
  }

  private def listNewPersistenceIds(events: Seq[Try[Seq[JournalEvent]]]): Future[Seq[String]] = {
    val uniquePids = events
      .flatMap(_.toOption.toSeq.flatMap(_.map(_.persistenceId)))
      .distinct

    journal
      .existsAll(queries.checkNewPersistenceIds(uniquePids))
      .thenApply(boolLst =>
        uniquePids.zip(boolLst.asScala.map(_.booleanValue())).flatMap {
          case (str, true) => Some(str)
          case _           => None
        }
      )
      .asScala

  }

  override def findHighestSequenceNr(
    persistenceId: String,
    fromSeqNr: Long,
  ): Future[Long] =
    journal
      .scanAll(queries.getHighestSequenceNr(persistenceId))
      .thenComposeAsync(mapper.parseHighestSequenceNr)
      .asScala

  override def getPersistentReprs(
    persistenceId: String,
    fromSequenceNr: Long,
    toSequenceNr: Long,
    max: Long,
  ): Future[Seq[PersistentRepr]] =
    journal
      .get(queries.getJournalRecords(persistenceId, fromSequenceNr, toSequenceNr, max))
      .thenComposeAsync(mapper.parsePersistentReprs(persistenceId))
      .asScala

  override def deleteRecords(
    persistenceId: String,
    toSequenceNr: Long,
  ): Future[Unit] =
    journal
      .get(queries.getSequenceNrsUpTo(persistenceId, toSequenceNr))
      .thenCompose(mapper.parseSequenceNrs)
      .thenCompose(seqNrs => journal.delete(queries.deleteSequenceNrs(persistenceId, seqNrs)))
      .thenApply(_ => ())
      .asScala
}
