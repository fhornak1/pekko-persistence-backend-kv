package ninja.slipping.pekko.persistence.hbase.stream.impl

import org.apache.hadoop.hbase.client.Result
import org.apache.pekko.stream.{Attributes, FlowShape, Inlet, Outlet}
import org.apache.pekko.stream.stage.{GraphStage, GraphStageLogic}

class RowParserStage[T] extends GraphStage[FlowShape[Result, T]] {
  val in: Inlet[Result] = Inlet("RowParser.in")
  val out: Outlet[T] = Outlet("RowParser.out")
  val err: Outlet[Throwable] = Outlet("RowParser.err")

  override def shape: FlowShape[Result, T] = FlowShape(in, out)
  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = ???
}
