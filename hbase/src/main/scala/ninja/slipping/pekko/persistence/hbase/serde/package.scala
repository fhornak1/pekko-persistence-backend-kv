package ninja.slipping.pekko.persistence.hbase

import org.apache.hadoop.hbase.Cell

import scala.util.Try

package object serde {

  def tryParseRow[T](
    cell: Cell
  )(
    implicit bp: BytesParser[T]
  ): Try[T] = bp.tryParse(cell.getRowArray, cell.getRowOffset, cell.getRowLength)

  def parseRow[T](
    cell: Cell
  )(
    implicit bp: BytesParser[T]
  ): T = bp.parse(cell.getRowArray, cell.getRowOffset, cell.getRowLength)

  def tryParseQualifier[T](
    cell: Cell
  )(
    implicit bp: BytesParser[T]
  ): Try[T] = bp.tryParse(cell.getQualifierArray, cell.getQualifierOffset, cell.getQualifierLength)

  def parseQualifier[T](
    cell: Cell
  )(
    implicit bp: BytesParser[T]
  ): T = bp.parse(cell.getQualifierArray, cell.getQualifierOffset, cell.getQualifierLength)

  def tryParseValue[T](
    cell: Cell
  )(
    implicit bp: BytesParser[T]
  ): Try[T] = bp.tryParse(cell.getValueArray, cell.getValueOffset, cell.getValueLength)

    def parseValue[T](
        cell: Cell
    )(
        implicit bp: BytesParser[T]
    ): T = bp.parse(cell.getValueArray, cell.getValueOffset, cell.getValueLength)
}
