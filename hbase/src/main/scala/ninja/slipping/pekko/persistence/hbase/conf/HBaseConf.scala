package ninja.slipping.pekko.persistence.hbase.conf

case class HBaseConf(
  journal: HBaseJournalConf,
  schema: HBaseSchemaConf,
  tag: HBaseTagConf,
)
