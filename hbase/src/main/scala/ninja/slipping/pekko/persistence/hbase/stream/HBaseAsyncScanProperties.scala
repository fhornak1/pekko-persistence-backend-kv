package ninja.slipping.pekko.persistence.hbase.stream

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration.*

object HBaseAsyncScanProperties {

  def default(
    suspendThreshold: Int = 10000,
    resumeThreshold: Int = 100,
    idleScanTimeout: FiniteDuration = 30.seconds,
  ): HBaseAsyncScanProperties =
    HBaseAsyncScanProperties(suspendThreshold, resumeThreshold, idleScanTimeout)
}

case class HBaseAsyncScanProperties(
  suspendThreshold: Int,
  resumeThreshold: Int,
  idleScanTimeout: FiniteDuration,
)
