package ninja.slipping.pekko.persistence.hbase.persistenceids.models

case class PersistenceIdModel(ordinal: Long, persistenceId: String)
