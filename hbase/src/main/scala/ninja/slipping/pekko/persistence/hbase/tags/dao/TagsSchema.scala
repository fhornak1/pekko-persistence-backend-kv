package ninja.slipping.pekko.persistence.hbase.tags.dao

object TagsSchema {
  val OrdinalsRow: Array[Byte] = Array[Byte](0, 0, 0, 0, 0, 0, 0, 0, '|', '_', 'o')
  
  val OrdinalColumn: Array[Byte] = Array[Byte]('o')
  val PreviousOrdinalColumn: Array[Byte] = Array[Byte]('p')
  val LastTransactionUUIDColumn: Array[Byte] = Array[Byte]('l')

  val PersistenceIdColumn: Array[Byte] = Array[Byte]('p')
  val SequenceNrColumn: Array[Byte]    = Array[Byte]('q')
  val RecordColumn: Array[Byte]         = Array[Byte]('r')

}
