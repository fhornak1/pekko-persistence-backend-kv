package ninja.slipping.pekko.persistence.hbase.stream.impl

import ninja.slipping.pekko.persistence.hbase.utils.parsing.{CellParser, ResultCellParser}
import org.apache.hadoop.hbase.client.Result
import org.apache.pekko.stream.{Attributes, FlowShape, Inlet, Outlet}
import org.apache.pekko.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler, StageLogging}

import scala.util.Try

class CellParserFlowStage[R, T](rowCellParser: ResultCellParser[R, T]) extends GraphStage[FlowShape[Result, T]] {
  val in: Inlet[Result] = Inlet("CellParserFlow.in")
  val out: Outlet[T] = Outlet("CellParserFlow.out")

  override def shape: FlowShape[Result, T] = FlowShape(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) with StageLogging {
      private var parser: Try[CellParser[T]] = _

      setHandler(out, new OutHandler {
        override def onPull(): Unit = {
          pull(in)
        }
      })

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          val result = grab(in)
          parser = rowCellParser.parseRow(result.rawCells().head).map(rowCellParser.createParser)
        }
      })

    }
}
