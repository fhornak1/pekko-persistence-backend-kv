package ninja.slipping.pekko.persistence.hbase.conf

case class HBasePersistenceIdOrdinalConf(family: Array[Byte], persistenceIdCQ: Array[Byte])
