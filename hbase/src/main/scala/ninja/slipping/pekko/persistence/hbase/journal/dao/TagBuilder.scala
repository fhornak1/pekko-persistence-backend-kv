package ninja.slipping.pekko.persistence.hbase.journal.dao

import ninja.slipping.pekko.persistence.hbase.Encoders
import ninja.slipping.pekko.presistence.kv.model.JournalEvent
import org.apache.hadoop.hbase.client.Put

class TagBuilder private (puts: Seq[Put], ord: Long, family: Array[Byte]) {

  def put(model: JournalEvent): TagBuilder = {
    val (lst, newOrd) = model.tags.foldLeft((List.empty[Put], ord)) {
      case ((lst, ord), tag) => (new Put(Encoders.encodeTag(tag, ord)) :: lst, ord + 1)
    }
    new TagBuilder(puts ++ lst, newOrd, family)
  }

}
