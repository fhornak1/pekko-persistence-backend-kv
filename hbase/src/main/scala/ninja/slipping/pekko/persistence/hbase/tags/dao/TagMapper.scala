package ninja.slipping.pekko.persistence.hbase.tags.dao

import ninja.slipping.pekko.persistence.hbase.Encoders
import ninja.slipping.pekko.persistence.hbase.tags.models.TagModel
import ninja.slipping.pekko.persistence.kv.utils.*
import org.apache.hadoop.hbase.client.Result
import org.apache.hadoop.hbase.util.Bytes

import scala.util.{Success, Try}

class TagMapper {

  def parseTagEntry(result: Result): Try[TagModel] = ???

  def parseHighestTagOrd(result: Result): Try[Long] = ???

  def parseMaxOrdinals(result: Result): Try[Map[String, Long]] =
    result.rawCells().map { cell =>
      for {
        tag <- Try(Encoders.toString(cell.getQualifierArray, cell.getQualifierOffset, cell.getQualifierLength))
        offset <- Try(Bytes.toLong(cell.getValueArray, cell.getValueOffset, cell.getValueLength))
      } yield (tag, offset)
    }.toSeq.sequence.map(_.toMap)
    
  def parseLastCommittedOrdinal(result: Result): Try[Long] =
    result.rawCells().map { cell => 
      Try(Bytes.toLong(cell.getValueArray, cell.getValueOffset, cell.getValueLength))
    }.headOption.getOrElse(Success(0L))

  // hash(jrn|pid)|jrn|pid|ord => evt
  // hash(tt|tag):tt>tag:ord|p => pid
  // hash(tt|tag):tt>tag|ord|s => seq
  // hash(tt|tag):tt>tag|ord|e => evt [optional]
  // hash(ot):ot|ord|p => pid
  // hash(ot):ot|ord|s => seq
  // hash(ot):ot|ord|e => evt
}
