package ninja.slipping.pekko.persistence.hbase

package object utils {
  
  def tap[A, U](sideEffect: A => U)(elm: A): A = {
    sideEffect(elm)
    elm
  }
}
