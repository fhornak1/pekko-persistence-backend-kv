package ninja.slipping.pekko.persistence.hbase.utils

import ninja.slipping.pekko.persistence.hbase.utils.parsing.{CellParser, RowParser}
import org.apache.hadoop.hbase.Cell
import org.apache.hadoop.hbase.client.Result

import java.util.concurrent.{CompletableFuture, CompletionStage}
import scala.util.{Failure, Success, Try}
import java.util.List as JList

object Implicits {

  implicit class ResultOps(val result: Result) extends AnyVal {

    def latestCellOption(
      family: Array[Byte],
      qualifier: Array[Byte],
    ): Option[Cell] =
      Option(result.getColumnLatestCell(family, qualifier))

    def parseLatestCellOption[T](
      family: Array[Byte],
      qualifier: Array[Byte],
    )(
      implicit cp: CellParser[T]
    ): Option[Try[T]] =
      latestCellOption(family, qualifier).map(cp.apply)

    def parseLatestCell[T : CellParser](
      family: Array[Byte],
      qualifier: Array[Byte],
    ): Try[T] =
      parseLatestCellOption(family, qualifier)
        .getOrElse(Failure(new NoSuchElementException("Element not found in array")))

    def parseRow[T : RowParser]: Try[T] =
      parseRowOption[T]
        .getOrElse(Failure(new NoSuchElementException("Can not parse row value of empty result")))
    
    def parseRowOption[T](implicit rp: RowParser[T]): Option[Try[T]] =
      result.rawCells().headOption.map(rp.apply)
  }


  implicit class CellOps(val cell: Cell) extends AnyVal {

    def parse[T](implicit parser: CellParser[T]): Try[T] = parser(cell)
  }

  implicit class TryOps[T](val value: Try[T]) extends AnyVal {

    def toCompletedStage: CompletionStage[T] =
      value match {
        case Failure(exception) => CompletableFuture.failedStage(exception)
        case Success(value) => CompletableFuture.completedStage(value)
      }
  }

}
