package ninja.slipping.pekko.persistence.hbase.conf

case class HBaseTagConf(
  family: Array[Byte],
  persistenceIdCQ: Array[Byte],
  sequenceNrCQ: Array[Byte],
  recordCQ: Array[Byte],
)
