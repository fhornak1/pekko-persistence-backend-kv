package ninja.slipping.pekko.persistence.hbase.stream.errors

import scala.concurrent.TimeoutException

object Errors {

  def maxAttemptsExceeded(maxAttempts: Int): MaxAttemptsExceededException =
    new MaxAttemptsExceededException(s"Maximum amount of poll attempts exceeded: ${maxAttempts}")
  
  def waitingForOnNextTimedOut(tookMs: Long): TimeoutException =
    new TimeoutException(s"Waiting for scan results timed out. It took ${tookMs} [ms].")
}
