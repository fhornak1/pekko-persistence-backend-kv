package ninja.slipping.pekko.persistence.hbase

import org.apache.hadoop.hbase.util.{ ByteArrayHashKey, Bytes, MurmurHash3 }

import java.nio.charset.StandardCharsets
import scala.util.Try

object Encoders {
  private val IntBytes: Int        = Integer.BYTES
  private val LongBytes: Int       = java.lang.Long.BYTES
  private val SeparatorSize: Int   = 1
  private val Separator: Byte      = '|'
  private val IntPrefixLength: Int = IntBytes + SeparatorSize
  private val ZeroOffset: Int      = 0

  def encodePersistenceId(persistenceId: String): Array[Byte] = {
    val bytes = strToBytes(persistenceId)
    catIntAndBytes(murmur3(bytes), bytes)
  }

  private def catIntAndBytes(
    prefix: Int,
    bytes: Array[Byte],
  ): Array[Byte] = {
    val output = new Array[Byte](bytes.length + IntPrefixLength)
    Array.copy(bytes, 0, output, IntPrefixLength, bytes.length)
    output(4) = Separator
    intToBytes(prefix, output)
  }

  def decodePersistenceId(bytes: Array[Byte]): String =
    decodePersistenceId(bytes, 0, bytes.length)

  def decodePersistenceId(
    bytes: Array[Byte],
    offset: Int,
    length: Int,
  ): String =
    toString(bytes, offset + IntPrefixLength, length - IntPrefixLength)

  def encodeSequenceNr(sequenceNr: Long): Array[Byte] =
    longToBytes(sequenceNr, new Array[Byte](LongBytes))

  def encodePersistenceIdOrdinal(value: Long): Array[Byte] =
    longToBytes(Long.MaxValue - value, new Array[Byte](LongBytes))

  private def longToBytes(
    value: Long,
    output: Array[Byte],
    offset: Int = ZeroOffset,
  ): Array[Byte] = {
    intToBytes((value >>> Integer.SIZE).toInt, output, offset)
    intToBytes(value.toInt, output, offset + Integer.BYTES)
  }

  def decodeSequenceNr(
    bytes: Array[Byte],
    offset: Int = ZeroOffset,
    length: Int = LongBytes,
  ): Try[Long] =
    Try(Bytes.toLong(bytes, offset, length))

  def decodePersistenceIdOrdinal(
    bytes: Array[Byte],
    offset: Int = ZeroOffset,
    length: Int = LongBytes,
  ): Try[Long] =
    Try(Bytes.toLong(bytes, offset, length)).map(Long.MaxValue - _)

  /** Create tag primary key for kv storages.
    *
    * Alignment:
    * {{{
    * murmur32(tag)|tag|ordinal
    * 4 bytes | N bytes | 8 bytes
    * }}}
    * @param tag
    * @param offset
    * @return
    */
  def encodeTag(
    tag: String,
    offset: Long,
  ): Array[Byte] = {
    val bytes  = strToBytes(tag)
    val output = new Array[Byte](IntPrefixLength + bytes.length + SeparatorSize + LongBytes)
    Array.copy(bytes, 0, intToBytes(murmur3(bytes), output), IntPrefixLength, bytes.length)
    output(IntBytes) = Separator
    output(IntPrefixLength + bytes.length) = Separator
    longToBytes(offset, output, IntPrefixLength + bytes.length + SeparatorSize)
  }

  def decodeTag(bytes: Array[Byte]): (String, Long) = {
    val tagLength = bytes.length - IntPrefixLength - LongBytes - SeparatorSize
    (toString(bytes, IntPrefixLength, tagLength), Bytes.toLong(bytes, bytes.length - LongBytes))
  }

  private def intToBytes(
    value: Int,
    output: Array[Byte],
    offset: Int = ZeroOffset,
  ): Array[Byte] = {
    output(offset + 3) = value.toByte
    output(offset + 2) = (value >> 8).toByte
    output(offset + 1) = (value >> 16).toByte
    output(offset) = (value >>> 24).toByte
    output
  }

  private def murmur3(bytes: Array[Byte]): Int =
    murmur3(bytes, 0, bytes.length)

  private def murmur3(
    bytes: Array[Byte],
    offset: Int,
    length: Int,
  ): Int =
    MurmurHash3.getInstance().hash(new ByteArrayHashKey(bytes, offset, length), 0)

  private[hbase] def strToBytes(str: String): Array[Byte] =
    str.getBytes(StandardCharsets.UTF_8)

  private[hbase] def toString(bytes: Array[Byte]): String =
    toString(bytes, 0, bytes.length)

  private[hbase] def toString(
    bytes: Array[Byte],
    offset: Int,
    length: Int,
  ): String =
    new String(bytes, offset, length, StandardCharsets.UTF_8)
}
