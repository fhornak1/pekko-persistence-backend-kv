package ninja.slipping.pekko.persistence.hbase.stream.impl

import ninja.slipping.pekko.persistence.hbase.stream.HBaseAsyncScanProperties
import org.apache.hadoop.hbase.client.{ AdvancedScanResultConsumer, AsyncTable, Result, Scan }
import org.apache.pekko.stream.stage.{ GraphStage, GraphStageLogic, OutHandler, StageLogging }
import org.apache.pekko.stream.{ Attributes, Outlet, SourceShape }

import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration

object HBaseAsyncScanSourceStage {

  def apply(
    table: AsyncTable[? <: AdvancedScanResultConsumer],
    scan: Scan,
    properties: HBaseAsyncScanProperties,
  ) =
    new HBaseAsyncScanSourceStage(
      table.asInstanceOf[AsyncTable[AdvancedScanResultConsumer]],
      scan,
      properties.suspendThreshold,
      properties.resumeThreshold,
      properties.idleScanTimeout,
    )
}

private[stream] class HBaseAsyncScanSourceStage(
  table: AsyncTable[AdvancedScanResultConsumer],
  scan: Scan,
  maxQueueLength: Int,
  resumeThreshold: Int,
  idleScanTimeout: FiniteDuration,
) extends GraphStage[SourceShape[Result]] {

  require(table != null, "AsyncTable object must not be null")
  require(scan != null, "Scan request must not be null")
  require(maxQueueLength > 0, "maxQueueLength must be positive integer (> 0)")
  require(resumeThreshold >= 0, "resumeThreshold must be non-negative integer.")
  require(
    resumeThreshold <= maxQueueLength,
    "resumeThreshold must be less or equal to maxQueueLength",
  )

  val out: Outlet[Result] = Outlet("HBaseAsyncScanSource.out")

  override def shape: SourceShape[Result] = SourceShape(out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) with StageLogging {
      private val controller    = new HBaseAsyncScanCallback.Controller()
      private val queue         = mutable.Queue[Result]()
      private var askedFor: Int = 0

      override def preStart(): Unit = {

        val onNext = getAsyncCallback[Array[Result]] { results =>
          queue.enqueueAll(results)

          pushRequested()

          if (queue.length > maxQueueLength) {
            controller.signalSuspend()
          }
        }

        val onFailure = getAsyncCallback[Throwable] { throwable =>
          emitMultiple(out, queue.dequeueAll(_ => true), () => failStage(throwable))
        }

        val onComplete = getAsyncCallback[Unit] { _ =>
          emitMultiple(out, queue.dequeueAll(_ => true), () => completeStage())
        }

        table.scan(
          scan,
          new HBaseAsyncScanCallback(onNext, onFailure, onComplete, controller, idleScanTimeout),
        )
      }

      override def postStop(): Unit =
        controller.signalClose()

      setHandler(
        out,
        new OutHandler {
          override def onPull(): Unit = {
            askedFor += 1
            pushRequested()
          }

          override def onDownstreamFinish(cause: Throwable): Unit = {
            controller.signalClose()
            super.onDownstreamFinish(cause)
          }
        },
      )

      private def pushRequested(): Unit = {
        while (askedFor > 0 && queue.nonEmpty) {
          askedFor -= 1
          push(out, queue.dequeue())
        }

        if (queue.isEmpty || queue.length <= resumeThreshold) {
          controller.resume()
        }
      }
    }
}
