package ninja.slipping.pekko.persistence.hbase.persistenceids.dao

import ninja.slipping.pekko.persistence.hbase.persistenceids.models.PersistenceIdModel
import org.apache.hadoop.hbase.client.{AdvancedScanResultConsumer, AsyncTable}

import java.util.concurrent.{CompletableFuture, CompletionStage}
import scala.concurrent.Future
import scala.jdk.FutureConverters.*
import scala.jdk.CollectionConverters.*

class PersistenceIdDao {

  private val table: AsyncTable[AdvancedScanResultConsumer] = ???
  private val queries: PersistenceIdQueries = ???
  private val mapper: PersistenceIdMapper = ???

  def putPersistenceIds(persistenceIds: Seq[String]): Future[Unit] = {
    allocateOrdinals(persistenceIds.size)
      .thenCompose(writePersistenceIds(persistenceIds))
      .asScala
  }

  private def allocateOrdinals(size: Int): CompletableFuture[Long] =
    table.increment(queries.incrementHighestOrdinal(size))
      .thenCompose(mapper.parseHighestPersistenceId)

  private def writePersistenceIds(persistenceIds: Seq[String])(maxOrdinal: Long): CompletionStage[Unit] = {
    val fromOrdinal = maxOrdinal - persistenceIds.size
    val lst = persistenceIds
      .zipWithIndex
      .map{ case (pid, idx) => PersistenceIdModel(fromOrdinal + idx, pid)}
      .map(queries.put)
      .asJava
    table.putAll(lst).thenApply(_ => ())
  }

  def scanNewPersistenceIds(fromOrdinal: Long, toOrdinal: Long): Future[Seq[PersistenceIdModel]] = ???

}
