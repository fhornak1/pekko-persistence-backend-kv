package ninja.slipping.pekko.persistence.hbase.persistenceids.dao

import ninja.slipping.pekko.persistence.hbase.Encoders
import ninja.slipping.pekko.persistence.hbase.conf.HBasePersistenceIdOrdinalConf
import ninja.slipping.pekko.persistence.hbase.persistenceids.models.PersistenceIdModel
import org.apache.hadoop.hbase.client.{Get, Increment, Put, Scan}

class PersistenceIdQueries(conf: HBasePersistenceIdOrdinalConf) {
  import PersistenceIdQueries.*

  def getHighestOrdinal: Get =
    new Get(OrdinalIncRow)
      .addColumn(conf.family, OrdinalNumCQ)

  def incrementHighestOrdinal(inc: Long): Increment =
    new Increment(OrdinalNumCQ).addColumn(conf.family, OrdinalNumCQ, inc)

  def scanRowsFromOrdinal(ordinal: Long): Scan = ???

  def scanRows(fromOrdinal: Long, toOrdinal: Long): Scan = ???

  def put(model: PersistenceIdModel): Put =
    new Put(Encoders.encodePersistenceIdOrdinal(model.ordinal))
      .addColumn(conf.family, conf.persistenceIdCQ, Encoders.strToBytes(model.persistenceId))
}

object PersistenceIdQueries {
  val OrdinalIncRow: Array[Byte] = Array[Byte](0, 0, 0, 0, 0, 0, 0, 0, '|', 'o', 'r', 'd')
  val OrdinalNumCQ: Array[Byte] = Array[Byte]('h', 'i')
  val StopRow: Array[Byte] = Array[Byte](-1, -1, -1, -1, -1, -1, -1, -1)
}
