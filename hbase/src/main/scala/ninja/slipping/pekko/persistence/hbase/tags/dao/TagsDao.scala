package ninja.slipping.pekko.persistence.hbase.tags.dao

import ninja.slipping.pekko.presistence.kv.model.JournalEvent
import org.apache.hadoop.hbase.client.{ AdvancedScanResultConsumer, AsyncTable, Put, Result }
import ninja.slipping.pekko.persistence.hbase.utils.Implicits.*
import org.apache.pekko.Done

import java.util.concurrent.{ CompletableFuture, CompletionStage }
import java.util.{ ArrayList as JArrayList, List as JList }
import scala.concurrent.Future
import scala.util.Try
import scala.jdk.FutureConverters.*

class TagsDao {

  val table: AsyncTable[AdvancedScanResultConsumer] = ???
  val queries: TagQueries                           = ???
  val mapper: TagMapper                             = ???
  
  def writeTags(tag: String, ordinal: Long, events: Seq[JournalEvent]): Future[Done] =
    table
      .putAll(queries.putEvents(tag, events, ordinal))
      .thenCompose(_ => CompletableFuture.completedStage(Done))
      .asScala

  def latestWrittenTagOrdinal(tag: String): Future[Long] =
    table
      .get(queries.getLastCommittedOrdinal(tag))
      .thenCompose(res => mapper.parseLastCommittedOrdinal(res).toCompletedStage)
      .asScala

  def commitHighestOrdinal(
    tag: String,
    ordinal: Long,
    previous: Long,
  ): Future[Done] =
    table
      .checkAndMutate(queries.checkAndMutateHighestOrdinal(tag, previous, ordinal))
      .thenCompose { camRes =>
        if (camRes.isSuccess) CompletableFuture.completedStage(Done)
        else
          CompletableFuture.failedFuture(
            new IllegalStateException("Failed to commit highest ordinal")
          )
      }
      .asScala

}
