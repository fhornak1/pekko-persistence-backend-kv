package ninja.slipping.pekko.persistence.hbase.helpers

import org.apache.hadoop.hbase.client.{ AdvancedScanResultConsumer, Cursor }

import java.util.Optional
import java.util.concurrent.atomic.AtomicReference

object ScanControllerTestKit {
  sealed trait CalledMethods

  final case object Suspend   extends CalledMethods
  final case object Terminate extends CalledMethods
  final case object Resume    extends CalledMethods
}

class ScanControllerTestKit
    extends AdvancedScanResultConsumer.ScanController
    with AdvancedScanResultConsumer.ScanResumer {
  import ScanControllerTestKit.*
  @volatile private var suspended  = false
  @volatile private var terminated = false

  private val calls = new AtomicReference(List.empty[CalledMethods])

  override def suspend: AdvancedScanResultConsumer.ScanResumer = {
    if (isStalled) throw new AssertionError("Suspending suspended or terminated scan!")
    suspended = true
    calls.updateAndGet(Suspend :: _)
    this
  }

  override def terminate(): Unit = {
    if (isStalled) throw new AssertionError("Terminating suspended or terminated scan!")
    calls.updateAndGet(Terminate :: _)
    terminated = true
  }

  override def cursor: Optional[Cursor] = Optional.empty

  override def resume(): Unit = {
    suspended = false
    calls.updateAndGet(Resume :: _)
  }

  def isPublishing = !isStalled

  private def isStalled = suspended || terminated

  def isRunning: Boolean = !terminated

  def isSuspended: Boolean = suspended
  def isTerminated         = terminated

  def getCalls: List[CalledMethods] =
    calls.get().reverse
}
