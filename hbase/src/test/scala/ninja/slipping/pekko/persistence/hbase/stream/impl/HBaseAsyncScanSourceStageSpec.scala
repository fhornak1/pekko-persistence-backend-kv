package ninja.slipping.pekko.persistence.hbase.stream.impl

import ninja.slipping.pekko.persistence.hbase.helpers.{ ResultTestHelpers, ScanExecutionTestKit }
import org.apache.hadoop.hbase.client.{ AdvancedScanResultConsumer, AsyncTable, Scan }
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import org.apache.pekko.stream.testkit.scaladsl.TestSink
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatestplus.mockito.MockitoSugar.mock

import scala.concurrent.duration.*

class HBaseAsyncScanSourceStageSpec extends AnyFlatSpec with Matchers with ResultTestHelpers {
  implicit private val system: ActorSystem = ActorSystem("TestActorSystem")

  private val tableMock = mock[AsyncTable[AdvancedScanResultConsumer]]

  "HBaseAsyncScanSourceStage" should "request first element" in {
    ScanExecutionTestKit
      .builder
      .thenWait(300.millis)
      .thenSend(Array(mkResult("row1", "value1"), mkResult("row2", "value2")))
      .thenWait(10.millis)
      .thenComplete()
      .tableWithStubbedScan(tableMock)

    val sourceUnderTest = Source.fromGraph(sourceStage())

    sourceUnderTest
      .map(parseSingleCellResult)
      .runWith(TestSink())
      .ensureSubscription()
      .requestNext(toRowString("row1", "value1"))
  }

  it should "read all elements and complete stage" in {
    ScanExecutionTestKit
      .builder
      .thenWait(300.millis)
      .thenSend(Array(mkResult("row1", "value1"), mkResult("row2", "value2")))
      .thenWait(10.millis)
      .thenSend(Array(mkResult("row3", "3"), mkResult("row4", "4")))
      .thenWait(30.millis)
      .thenSend(Array(mkResult("row5", "eee")))
      .thenWait(700.millis)
      .thenComplete()
      .tableWithStubbedScan(tableMock)

    val sourceUnderTest = Source.fromGraph(sourceStage())

    sourceUnderTest
      .map(parseSingleCellResult)
      .runWith(TestSink())
      .ensureSubscription()
      .request(10)
      .expectNext(
        toRowString("row1", "value1"),
        toRowString("row2", "value2"),
        toRowString("row3", "3"),
        toRowString("row4", "4"),
        toRowString("row5", "eee"),
      )
      .expectComplete()
  }

  it should "return failure after all elements" in {
    val error = new Exception("Test exception")
    ScanExecutionTestKit
      .builder
      .thenWait(300.millis)
      .thenSend(Array(mkResult("row1", "value1"), mkResult("row2", "value2")))
      .thenWait(10.millis)
      .thenSend(Array(mkResult("row3", "3"), mkResult("row4", "4")))
      .thenWait(30.millis)
      .thenSend(Array(mkResult("row5", "eee")))
      .thenWait(700.millis)
      .thenFail(error)
      .tableWithStubbedScan(tableMock)

    val sourceUnderTest = Source.fromGraph(sourceStage())

    sourceUnderTest
      .map(parseSingleCellResult)
      .runWith(TestSink())
      .ensureSubscription()
      .request(10)
      .expectNext(
        toRowString("row1", "value1"),
        toRowString("row2", "value2"),
        toRowString("row3", "3"),
        toRowString("row4", "4"),
        toRowString("row5", "eee"),
      )
      .expectError(error)
  }

  private def sourceStage(
    maxQueueLen: Int = 1,
    resumeThreshold: Int = 0,
    idleTimeout: FiniteDuration = 30.seconds,
  ): HBaseAsyncScanSourceStage =
    new HBaseAsyncScanSourceStage(tableMock, new Scan(), maxQueueLen, resumeThreshold, idleTimeout)
}
