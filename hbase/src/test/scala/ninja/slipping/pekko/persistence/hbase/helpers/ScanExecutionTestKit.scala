package ninja.slipping.pekko.persistence.hbase.helpers

import ninja.slipping.pekko.persistence.hbase.helpers.ScanExecutionTestKit.Scenario
import org.apache.hadoop.hbase.client.{ AdvancedScanResultConsumer, AsyncTable, Result }
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.doAnswer
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Stubber
import org.scalatestplus.mockito.MockitoSugar.mock

import scala.collection.{ immutable, mutable }
import scala.concurrent.duration.FiniteDuration

object ScanExecutionTestKit {

  sealed trait ScanState
  final case object Running    extends ScanState
  final case object Suspended  extends ScanState
  final case object Terminated extends ScanState

  sealed trait Scenario {
    def exec(
      consumer: AdvancedScanResultConsumer,
      controller: AdvancedScanResultConsumer.ScanController,
    ): Unit
  }

  final class SendResults(results: Array[Result]) extends Scenario {
    override def exec(
      consumer: AdvancedScanResultConsumer,
      controller: AdvancedScanResultConsumer.ScanController,
    ): Unit =
      consumer.onNext(results, controller)
  }

  final class SendHeartbeat extends Scenario {
    override def exec(
      consumer: AdvancedScanResultConsumer,
      controller: AdvancedScanResultConsumer.ScanController,
    ): Unit =
      consumer.onHeartbeat(controller)
  }

  final class Wait(durationMs: Long) extends Scenario {
    override def exec(
      consumer: AdvancedScanResultConsumer,
      controller: AdvancedScanResultConsumer.ScanController,
    ): Unit =
      Thread.sleep(durationMs)
  }

  final class Fail(exception: Throwable) extends Scenario {
    override def exec(
      consumer: AdvancedScanResultConsumer,
      controller: AdvancedScanResultConsumer.ScanController,
    ): Unit =
      consumer.onError(exception)
  }

  final object Complete extends Scenario {
    override def exec(
      consumer: AdvancedScanResultConsumer,
      controller: AdvancedScanResultConsumer.ScanController,
    ): Unit =
      consumer.onComplete()
  }

  class TestException extends Exception("Generic test exception")

  def builder: Builder = new Builder

  class Builder {
    private val execQueue: mutable.Queue[Scenario] = mutable.Queue()

    def thenWait(duration: FiniteDuration): Builder = {
      execQueue.enqueue(new Wait(duration.toMillis))
      this
    }

    def thenSend(results: Array[Result]): Builder = {
      execQueue.enqueue(new SendResults(results))
      this
    }

    def thenHeartbeat(): Builder = {
      execQueue.enqueue(new SendHeartbeat)
      this
    }

    def thenFailGeneric(): ScanExecutionTestKit =
      thenFail(new TestException)

    def thenFail(ex: Throwable): ScanExecutionTestKit = {
      execQueue.enqueue(new Fail(ex))
      new ScanExecutionTestKit(immutable.Queue.from(execQueue))
    }

    def thenComplete(): ScanExecutionTestKit = {
      execQueue.enqueue(Complete)
      new ScanExecutionTestKit(immutable.Queue.from(execQueue))
    }

  }
}
class ScanExecutionTestKit private (scenarios: immutable.Queue[Scenario]) {

  def createScenarioStubber(controller: ScanControllerTestKit): Stubber =
    doAnswer { (invocation: InvocationOnMock) =>
      val consumer = invocation.getArgument[AdvancedScanResultConsumer](1)

      val thd = new Thread(() => {
        var sc = scenarios

        while (sc.nonEmpty && controller.isRunning)
          if (controller.isSuspended) {
            Thread.sleep(10)
          }
          else {
            val (scenario, nextQ) = sc.dequeue

            scenario.exec(consumer, controller)

            sc = nextQ
          }

      })

      thd.start()
    }

  def tableWithStubbedScan(tableMock: AsyncTable[AdvancedScanResultConsumer])
    : ScanControllerTestKit = {
    val controller = new ScanControllerTestKit
    createScenarioStubber(controller)
      .when(tableMock)
      .scan(any(), any())

    controller
  }

}
