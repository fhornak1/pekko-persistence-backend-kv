package ninja.slipping.pekko.persistence.hbase.helpers

import com.nimbusds.jose.util.StandardCharset
import org.apache.hadoop.hbase.{ Cell, CellBuilderFactory, CellBuilderType }
import org.apache.hadoop.hbase.client.Result

trait ResultTestHelpers {

  protected def mkResult(
    row: String,
    value: String,
  ): Result = {
    val cell = CellBuilderFactory
      .create(CellBuilderType.SHALLOW_COPY)
      .setRow(row.getBytes(StandardCharset.UTF_8))
      .setFamily(Array[Byte]('f'))
      .setQualifier(Array[Byte]('c'))
      .setValue(value.getBytes(StandardCharset.UTF_8))
      .setType(Cell.Type.Put)
      .setTimestamp(1L)
      .build()
    Result.create(Array(cell))
  }

  protected def parseSingleCellResult(result: Result): String = {
    val cell     = result.rawCells()(0)
    val rowStr   =
      new String(cell.getRowArray, cell.getRowOffset, cell.getRowLength, StandardCharset.UTF_8)
    val valueStr = new String(
      cell.getValueArray,
      cell.getValueOffset,
      cell.getValueLength,
      StandardCharset.UTF_8,
    )

    toRowString(rowStr, valueStr)
  }
  
  protected def toRowString(rowStr: String, valueStr: String): String =
    s"$rowStr qualifier=f:c value=$valueStr"
}
