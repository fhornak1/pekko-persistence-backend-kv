import sbt.*

object Dependencies {

  object Log4j {
    val group   = "org.apache.logging.log4j"
    val version = "2.20.0"

    lazy val `log4j-slf4j-impl` = group % "log4j-slf4j-impl" % version
    lazy val `log4j-api`        = group % "log4j-api"        % version
    lazy val `log4j-core`       = group % "log4j-core"       % version

    lazy val testBundle = Seq(
      `log4j-api`        % Test,
      `log4j-core`       % Test,
      `log4j-slf4j-impl` % Test,
    )
  }

  object Common {

    lazy val `slf4j-api` = "org.slf4j" % "slf4j-api" % "1.7.36"
  }
  object Java {
    val jupiterGroup = "org.junit.jupiter"
    val jupiterEngineGroup = "org.junit.platform"

    val jupiterVersion       = "5.10.0"
    val jupiterEngineVersion = "1.10.0"

    lazy val lombok = "org.projectlombok" % "lombok" % "1.18.26"

    lazy val `jupiter-interface`       = "net.aichler" %% "jupiter-interface"
    lazy val `junit-platform-launcher` =
      jupiterEngineGroup % "junit-platform-launcher" % jupiterEngineVersion
    lazy val `junit-platform-commons` =
      jupiterEngineGroup % "junit-platform-commons" % jupiterEngineVersion
    lazy val `junit-jupiter-engine` = jupiterGroup % "junit-jupiter-engine" % jupiterVersion
    lazy val `junit-jupiter-api`    = jupiterGroup % "junit-jupiter-api"    % jupiterVersion

    lazy val `junit-jupiter-tests` = Seq(
      `junit-jupiter-api`       % Test,
      `junit-jupiter-engine`    % Test,
      `junit-platform-launcher` % Test,
      `junit-platform-commons`  % Test,
    )
  }

  object Scala {
    val scalatestVersion     = "3.2.17"
    val scalatestplusVersion = "3.2.17.0"

    lazy val `scala-logging` = "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5"

    lazy val scalatest      = "org.scalatest"     %% "scalatest"    % scalatestVersion
    lazy val `mockito-4-11` = "org.scalatestplus" %% "mockito-4-11" % scalatestplusVersion
    lazy val `junit-5-10`   = "org.scalatestplus" %% "junit-5-10"   % scalatestplusVersion

    lazy val scalaTest = Seq(
      scalatest      % Test,
      `mockito-4-11` % Test,
      `junit-5-10`   % Test,
    )
  }

  object Protobuf {
    val protoVersion  = "3.21.5"
    val proto2Version = "2.5.0" // HBase
    val group         = "com.google.protobuf"

    lazy val `protobuf-java`  = group % "protobuf-java" % protoVersion
    lazy val `protobuf-java2` = group % "protobuf-java" % proto2Version

    lazy val `protoc`  = group % "protoc" % protoVersion
    lazy val `protoc2` = group % "protoc" % proto2Version

    lazy val `scalapb-runtime` =
      "com.thesamet.scalapb" %% "scalapb-runtime" % scalapb.compiler.Version.scalapbVersion

  }

  object Pekko {
    val version: String = "1.0.1"
    val group           = "org.apache.pekko"

    lazy val `pekko-actor`             = group %% "pekko-actor"             % version
    lazy val `pekko-persistence`       = group %% "pekko-persistence"       % version
    lazy val `pekko-persistence-query` = group %% "pekko-persistence-query" % version

    lazy val `pekko-testkit`             = group %% "pekko-testkit"             % version
    lazy val `pekko-persistence-testkit` = group %% "pekko-persistence-testkit" % version
    lazy val `pekko-persistence-tck`     = group %% "pekko-persistence-tck"     % version
    lazy val `pekko-stream-testkit`      = group %% "pekko-stream-testkit"      % version

    lazy val persistenceTestKit = Seq(
      `pekko-testkit`             % Test,
      `pekko-persistence-testkit` % Test,
      `pekko-persistence-tck`     % Test,
      `pekko-stream-testkit`      % Test,
    )
  }

  object HBase {
    val version = "2.5.3"
    val group   = "org.apache.hbase"

    lazy val `hbase-client`        = group % "hbase-client"        % version
    lazy val `hbase-server`        = group % "hbase-server"        % version
    lazy val `hbase-common`        = group % "hbase-common"        % version
    lazy val `hbase-shaded-client` = group % "hbase-shaded-client" % version

    lazy val `hbase-it`           = group % "hbase-it"           % version
    lazy val `hbase-testing-util` = group % "hbase-testing-util" % version

    lazy val coprocessorCompileOnly = Seq(`hbase-common`, `hbase-server`)
  }

  object TiKV {
    lazy val `tikv-client-java` = "org.tikv" % "tikv-client-java" % "3.3.5"
  }

  object Misc {
    lazy val `commons-codec` = "org.apache.commons" % "commons-codec" % "1.6"
  }
}
