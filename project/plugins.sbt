addSbtPlugin("com.thesamet" % "sbt-protoc" % "1.0.6")
addSbtPlugin("net.aichler" % "sbt-jupiter-interface" % "0.11.1")
addSbtPlugin("com.thoughtworks.sbt" % "delombokjavadoc" % "1.0.0")

libraryDependencies += "com.thesamet.scalapb" %% "compilerplugin" % "0.11.13"
